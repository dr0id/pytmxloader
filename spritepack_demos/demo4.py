"""Billybob's Quest for Anonymity

To play:

    Run: python demo4.py.

    Use the mouse in the game menu.

    Use the cursor keys in game to complete quests. Game time elapses only when
    a cursor key is pressed. Take your time, read the game messages. Hope they
    are worth a chuckle.

    The game shuffles the quest order and awards random gear during the
    adventure, so identical game stories should be rare.

Bugs:

    There are no skeleton skins for the thrust and spellcast tilesets. When
    choosing the skeleton you will see armor and weapons but no body during the
    spear and wet match quests.
"""


import glob
import random

import pygame
from pygame.locals import *

from pytmxloader.spritesheetlib import SpritesheetLib10, DummyLogger, FileInfo


spritesheetlib = SpritesheetLib10(DummyLogger())

screen = None
screen_rect = None
clock = None
max_fps = None
dt = None
gui_font = None
text_font = None


class AutoNestedDict(dict):
    """
    a = AutoNestedDict()
    a[1][2][3][4][5][6][7] = []
    a[1][2][3][4][5][6][7].append(9999)

    http://stackoverflow.com/questions/651794/whats-the-best-way-to-initialize-a-dict-of-dicts-in-python
    """

    def __getitem__(self, item):
        return self.__mygetitem(item)

    def __mygetitem_auto(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value

    def __mygetitem_noauto(self, item):
        return dict.__getitem__(self, item)

    __mygetitem = __mygetitem_auto

    def disable_auto(self):
        self.__mygetitem = self.__mygetitem_noauto

    def enable_auto(self):
        self.__mygetitem = self.__mygetitem_auto

# Structure:
#   sprites_dict[action][facing][pose][skin][style][name] = [sprite0, sprite1, ...]
sprites_dict = AutoNestedDict()


def get_sprites(action='walk', facing='down', pose='walkcycle', skin='BODY', style='male', name='male'):
    """dumb wrapper just to aid myself with PyCharm ^p hints =)
    """
    return sprites_dict[action][facing][pose][skin][style][name]


def group_by_afpssn(sprites):
    # 'afpssn' == sprites_dict[action][facing][pose][skin][style][name] = [sprite0, sprite1, ...]
    # action: walk stand
    # facing: up down left right
    # pose:   walkcycle thrust spellcast slash hurt combat_dummy bow
    # skin:   BEHIND BODY HEAD LEGS BELT TORSO FEET HANDS WEAPON
    # style:  arrow bow chain_armor dagger hair leather leather_armor male pants plate_armor quiver robe
    #         robe_shirt rope shield shoes skeleton spear staff style
    # name:   ...
    for sprite in sprites:
        p = sprite.properties
        action = p['action']
        facing = p['facing']
        pose = p['pose']
        skin = p['skin']
        style = p['style']
        name = p['name']

        poses = sprites_dict[action][facing][pose]

        if not poses[skin][style][name]:
            poses[skin][style][name] = []

        poses[skin][style][name].append(sprite)


def load_all_sprite_frames():
    subdirs = 'walkcycle thrust spellcast slash hurt combat_dummy bow'.split(' ')
    for subdir in subdirs:
        for filename in glob.glob('{}/*.png.sdef9'.format(subdir)):
            sprites = spritesheetlib.load_spritesheet(FileInfo(filename))
            group_by_afpssn(sprites)
    sprites_dict.disable_auto()
    # print('done')
    # print(sprites_dict['stand']['down']['walkcycle']['BODY']['male']['male'])
    # print(sprites_dict['walk']['down']['walkcycle']['BODY']['male']['male'])
    # quit()


class Animation(object):

    _props = 'action facing pose'

    def __init__(self, action, facing, pose):
        """
        base_sprites: Need to know how many frames, and view some properties.
        """
        self._action = action
        self._facing = facing
        self._pose = pose

        self.sprites = None
        self.index = 0
        self.count = 0
        self.elapsed = 0
        self.time_per_frame = 0.010  # seconds
        self.reset()

    def get_sprite(self, skin='BODY', style='male', name='male'):
        try:
            sprites = [s for s in self.sprites[skin][style][name] if s.properties['action'] == self._action]
            return sprites[self.index]
        except (KeyError, IndexError):
            return None

    def set_prop(self, prop, value):
        assert prop in self._props
        setattr(self, '_' + prop, value)
        self._update_index()

    def reset(self):
        self.elapsed = 0
        self.index = 0
        self.sprites = sprites_dict[self._action][self._facing][self._pose]

        sprites = [s for s in self.sprites['BODY']['male']['male'] if s.properties['action'] == self._action]
        self.count = len(sprites)
        self.time_per_frame = sprites[0].properties['time_per_frame'] / 1000.0  # convert from ms to seconds

    def _update_index(self):
        self.sprites = sprites_dict[self._action][self._facing][self._pose]
        sprites = [s for s in self.sprites['BODY']['male']['male'] if s.properties['action'] == self._action]
        self.count = len(sprites)
        if self.index >= self.count:
            self.index = 0

    def update(self, dt):

        self.elapsed += dt

        # skip frames (will hopefully never happen)
        frames = 0
        while self.elapsed >= self.time_per_frame:
            self.elapsed -= self.time_per_frame  # this would be a nicer animation, but makes animations go out of sync
            frames += 1
            self.elapsed = 0
            break  # just keep it simple and synchronized with other animations

        if frames > 0:
            self.index += frames
            if self.index >= self.count:
                # end of animation
                pass
            self.index %= self.count

            sprite = self.get_sprite('BODY', 'male', 'male')
            self.time_per_frame = sprite.properties['time_per_frame'] / 1000.0  # convert from ms to seconds


class Character(object):

    BODY = 'male skeleton'
    HAIR = 'blonde'

    def __init__(self, action='stand', facing='down', pose='walkcycle', body=None):
        self.body = 'BODY', body, body
        self.quiver = None
        self.belt = None
        self.feet = None
        self.hands = None
        self.hair = None
        self.head = None
        self.legs = 'LEGS', 'pants', 'greenish' if body == 'male' else None
        self.torso = None
        self.weapon = None
        self.shield = None

        self.action = action
        self.facing = facing
        self.pose = pose
        self.rect = Rect(0, 0, 1, 1)

        # Layer order:
        # behind (self.weapons: quiver)
        # body (self.body)
        # hair (self.hair)
        # armor (self.armor: *)
        # weapon  (self.weapons: equipped | arrows)
        # shield (self.shield: shield)

        self.anim = Animation(self.action, self.facing, self.pose)

    def equip(self, attr):
        skin, style, name = attr
        if skin == 'BODY':
            self.body = attr
        elif skin == 'BEHIND':
            self.quiver = attr
        elif skin == 'BELT':
            self.belt = attr
        elif skin == 'FEET':
            self.feet = attr
        elif skin == 'HANDS':
            self.hands = attr
        elif skin == 'HEAD':
            if style == 'hair':
                self.hair = attr
            else:
                self.head = attr
        elif skin == 'LEGS':
            self.legs = attr
        elif skin == 'TORSO':
            self.torso = attr
        elif skin == 'WEAPON':
            # WEAPON changes the pose
            if style == 'shield':
                self.shield = attr
                if self.weapon:
                    self.change_pose('thrust')
                else:
                    self.change_pose('walkcycle')
            else:
                self.weapon = attr
                if style == 'spear':
                    self.change_pose('thrust')
                elif style == 'staff':
                    self.change_pose('thrust')
                elif style == 'dagger':
                    self.change_pose('slash')
                elif style == 'bow':
                    self.change_pose('bow')
                elif style == 'wet match':
                    self.change_pose('spellcast')

    def change_pose(self, pose):
        self.pose = pose
        self.anim.set_prop('pose', pose)

    def face(self, direction):
        assert direction in 'left right up down'
        self.anim.set_prop('facing', direction)

    def moving(self, boolean):
        if boolean and self.anim._action != 'walk':
            self.anim.set_prop('action', 'walk')
            self.action = 'walk'
        elif self.anim._action != 'stand':
            self.anim.set_prop('action', 'stand')
            self.action = 'stand'

    def update(self, dt):
        self.anim.update(dt)

    def draw(self):
        # Layer order:
        # behind (self.weapons: quiver)
        # body (self.body)
        # hair (self.hair)
        # armor (self.armor: *)
        # weapon  (self.weapons: equipped | arrows)
        # activity (self.activity: pose | moving | facing)

        self.draw_layer(self.quiver)
        self.draw_layer(self.body)
        self.draw_layer(self.hair)
        self.draw_layer(self.head)
        self.draw_layer(self.torso)
        self.draw_layer(self.legs)
        self.draw_layer(self.belt)
        self.draw_layer(self.feet)
        self.draw_layer(self.hands)
        self.draw_layer(self.weapon)
        self.draw_layer(self.shield)

    def draw_layer(self, attr):
        if attr:
            skin, style, name = attr
            sprite = self.anim.get_sprite(skin, style, name)
            if sprite:
                image = sprite.image
                rect = self.rect
                rect.size = image.get_size()
                rect.center = screen_rect.center
                screen.blit(image, rect)


class BaseState:

    @staticmethod
    def enter(self):
        pass

    @staticmethod
    def think(self, dt):
        if self.char.action == 'walk':
            self.elapsed += dt
            if self.elapsed > self.msg_timer:
                self.erase_msg()
            if self.elapsed > self.next_encounter:
                self.level_up()

    @staticmethod
    def exit(self):
        pass


class WalkingState(BaseState):

    @staticmethod
    def enter(self):
        pygame.display.set_caption('Quest: WALKING AROUND')
        self.elapsed = 0.0
        if not self.msg:
            self.new_msg('One day Billybob was walking around...')
        self.next_encounter = 5.0


class BowState(BaseState):

    @staticmethod
    def enter(self):
        pygame.display.set_caption('Quest: USING A BOW')
        self.new_msg('Oo... you found a bow! Practice, practice.')
        self.elapsed = 0.0
        self.next_encounter = 5.0


class SlashState(BaseState):

    @staticmethod
    def enter(self):
        pygame.display.set_caption('Quest: USING A SLASHING WEAPON')
        self.new_msg('Oo... you found a dagger! Practice, practice.')
        self.elapsed = 0.0
        self.next_encounter = 5.0


class SpellState(BaseState):

    @staticmethod
    def enter(self):
        pygame.display.set_caption('Quest: FAILING AT MAGIC')
        self.new_msg('Oo... you found a wet match! Practice, practice.')
        self.elapsed = 0.0
        self.next_encounter = 5.0


class ThrustState(BaseState):

    @staticmethod
    def enter(self):
        pygame.display.set_caption('Quest: USING A POKER')
        self.new_msg('Oo... you found a spear! Practice, practice.')
        self.elapsed = 0.0
        self.next_encounter = 5.0


class FinaleState(BaseState):

    @staticmethod
    def enter(self):
        pygame.display.set_caption('Quest: BOSS ENCOUNTER')
        self.new_msg('Look busy, here come da boss!')
        self.elapsed = 0.0
        self.next_encounter = 5.0


class WinState(BaseState):

    @staticmethod
    def enter(self):
        pygame.display.set_caption('Quest: SINGING BIRDS')
        self.new_msg('There is peace in the land once again')
        self.elapsed = 0.0
        self.next_encounter = 5.0


class Game(object):

    def __init__(self, body):
        self.char = Character(body=body)
        self.running = False

        self.key_down = 0

        quests = 'bow slash spellcast thrust'.split()
        random.shuffle(quests)
        self.quests = ['walkcycle'] + quests + ['finale']
        self.next_encounter = 0.0

        self.msg = None
        self.msg_image = None
        self.msg_rect = None
        self.msg_timer = 0.0

        self.elapsed = 0
        self.current_state = WalkingState
        self.current_state.enter(self)

    def run(self):
        self.running = True
        while self.running:
            clock.tick(max_fps)
            self.get_events()
            self.update(dt)
            self.draw()

    def set_state(self, state):
        if state is not None and state != self.current_state:
            self.current_state.exit(self)
            self.current_state = state
            state.enter(self)

    def update(self, dt):
        state = self.current_state.think(self, dt)
        self.set_state(state)
        self.char.update(dt)

    def new_msg(self, text):
        self.msg = text
        self.msg_timer = 3.0
        self.msg_image = None
        self.msg_rect = None

    def erase_msg(self):
        self.msg = None
        self.msg_image = None
        self.msg_rect = None

    def level_up(self):
        new_state = None

        if self.current_state == WinState:
            self.running = False
            return

        elif self.current_state == FinaleState:
            # you won!
            new_state = WinState

        elif self.current_state == WalkingState:
            # get a new quest
            self.quests.pop(0)
            quest = self.quests[0]
            if quest == 'bow':
                new_state = BowState
                self.char.equip(('WEAPON', 'bow', 'bow'))
            elif quest == 'slash':
                new_state = SlashState
                self.char.equip(('WEAPON', 'dagger', 'dagger'))
            elif quest == 'spellcast':
                new_state = SpellState
                self.char.equip(('WEAPON', 'wet match', 'wet match'))
            elif quest == 'thrust':
                new_state = ThrustState
                # we'll ignore the staff, although it's available
                self.char.equip(('WEAPON', 'spear', 'spear'))
            elif quest == 'finale':
                new_state = FinaleState
                # maybe add BOSS combat dummy here? :)

        else:
            # find a random (skin, style, name) treasure from the sprites_dict
            sprites = sprites_dict['walk']['up'][self.char.pose]
            rand_skin = 'BODY'
            while rand_skin in ('BODY', 'WEAPON'):
                rand_skin = random.choice(sprites.keys())
            rand_style = 'male'
            while rand_style in ('male', 'skeleton'):
                rand_style = random.choice(sprites[rand_skin].keys())
            rand_name = 'male'
            while rand_name in ('male', 'skeleton'):
                rand_name = random.choice(sprites[rand_skin][rand_style].keys())

            # equip it
            self.char.equip((rand_skin, rand_style, rand_name))
            self.char.change_pose('walkcycle')

            # tell the player about it
            self.new_msg('Oo... you found a {} {} {}'.format(rand_skin, rand_style, rand_name))
            new_state = WalkingState

        # advance the state (quest)
        self.current_state.exit(self)
        self.current_state = new_state
        new_state.enter(self)

    def draw(self):
        screen.fill((0, 0, 0))
        self.draw_msg()
        self.char.draw()
        pygame.display.flip()

    def draw_msg(self):
        if self.msg:
            if not self.msg_image:
                self.msg_image, self.msg_rect = render_text(self.msg)
            screen.blit(self.msg_image, self.msg_rect)
        else:
            self.msg_image = None
            self.msg_rect = None

    def get_events(self):
        for e in pygame.event.get():
            if e.type == QUIT:
                self.quit()
            elif e.type == KEYDOWN:
                self.on_key_down(e.key, e.mod)
            elif e.type == KEYUP:
                self.do_key_up(e.key)

    def quit(self):
        self.running = False

    def on_key_down(self, key, mod):
        if key == K_ESCAPE:
            self.quit()
        elif key == K_UP:
            self.char.face('up')
            self.char.moving(True)
            self.key_down += 1
        elif key == K_DOWN:
            self.char.face('down')
            self.char.moving(True)
            self.key_down += 1
        elif key == K_LEFT:
            self.char.face('left')
            self.char.moving(True)
            self.key_down += 1
        elif key == K_RIGHT:
            self.char.face('right')
            self.char.moving(True)
            self.key_down += 1

    def do_key_up(self, key):
        if key in (K_UP, K_DOWN, K_LEFT, K_RIGHT):
            self.key_down -= 1
            if not self.key_down:
                self.char.moving(False)


class Menu(object):
    
    def __init__(self):
        self.running = False

    def run(self):
        self.running = True
        while self.running:
            pygame.display.set_caption("QUEST FOR ANONYMITY")

            screen.fill((0, 0, 0))
            im, rect = render_text('Use arrows keys in game')
            screen.blit(im, rect)
            pick('Ok')

            screen.fill((0, 0, 0))
            im, rect = render_text('Billybob was an unadventurous')
            screen.blit(im, rect)
            body = pick('Man Skeleton').lower()

            if body == 'man':
                body = 'male'
            game = Game(body)
            game.run()

            screen.fill((0, 0, 0))
            im, rect = render_text('Play again?')
            screen.blit(im, rect)
            again = pick('Yes No').lower()

            if again == 'no':
                self.running = False


def render_text(text):
    im = text_font.render(text, True, Color('green'))
    rect = im.get_rect(centerx=screen_rect.centerx, y=screen_rect.h // 3)
    return im, rect


def pick(choices):
    buttons = []
    total_width = 0
    for word in choices.split():
        im = gui_font.render(word, True, Color('#CCFF66'), Color('#660066'))
        rect = im.get_rect(y=screen_rect.centery)
        buttons.append((word, im, rect))
        total_width += rect.w

    spacing = 20
    total_width += spacing * (len(buttons) - 1)
    x = screen_rect.centerx - total_width // 2
    for word, im, rect in buttons:
        rect.x = x
        x += rect.w + spacing
        screen.blit(im, rect)

    pygame.display.flip()

    decision = None
    while not decision:
        for e in pygame.event.get():
            if e.type == QUIT or e.type == KEYDOWN and e.key == K_ESCAPE:
                quit()
            elif e.type == MOUSEBUTTONDOWN:
                for word, im, rect in buttons:
                    if rect.collidepoint(e.pos):
                        decision = word
                        break
    return decision


def main():
    global screen, screen_rect, clock, max_fps, dt, gui_font, text_font

    pygame.init()

    screen = pygame.display.set_mode((640, 480))
    screen_rect = screen.get_rect()
    clock = pygame.time.Clock()
    max_fps = 30
    assert max_fps > 0
    dt = 1.0 / max_fps

    gui_font = pygame.font.SysFont('sans', 32, bold=True, italic=True)
    text_font = pygame.font.SysFont('sans', 24, bold=True)

    load_all_sprite_frames()

    menu = Menu()
    menu.run()


if __name__ == '__main__':
    main()
