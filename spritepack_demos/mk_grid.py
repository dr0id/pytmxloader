#!/usr/bin/env python
# coding=utf-8

"""mk_grid.py - generate *.sdef for all the PNGs in the configurated subdiectories
"""

import glob
import os
import re
import json


# TIP: remove """ to allow PyCharm to check your syntax.

# most tilesheets
properties_9x4 = """{
    ':': {'pose': '%P', 'skin': '%S', 'style': '%T', 'name': '%N', 'snap_to_rest': true, 'time_per_frame':100.0 },
    '0:9': {'facing': 'up'},
    '9:18': {'facing': 'left'},
    '18:27': {'facing': 'down'},
    '27:36': {'facing': 'right'},
    '0': {'resting': true, 'moving': false, 'action': 'stand', 'speed_x': 0, 'speed_y': 0},
    '9': {'resting': true, 'moving': false, 'action': 'stand', 'speed_x': 0, 'speed_y': 0},
    '18': {'resting': true, 'moving': false, 'action': 'stand', 'speed_x': 0, 'speed_y': 0},
    '27': {'resting': true, 'moving': false, 'action': 'stand', 'speed_x': 0, 'speed_y': 0},
    '1:9': {'resting': false, 'moving': true, 'action': 'walk', 'speed_x': 0, 'speed_y': -10},
    '10:18': {'resting': false, 'moving': true, 'action': 'walk', 'speed_x': -10, 'speed_y': 0},
    '19:27': {'resting': false, 'moving': true, 'action': 'walk', 'speed_x': 0, 'speed_y': 10},
    '28:36': {'resting': false, 'moving': true, 'action': 'walk', 'speed_x': 10, 'speed_y': 0},
    'mode': 'update'
}"""

# thrust
properties_8x4 = """{
    ':': {'pose': '%P', 'skin': '%S', 'style': '%T', 'name': '%N', 'snap_to_rest': true, 'time_per_frame':100.0 },
    '0:8': {'facing': 'up'},
    '8:16': {'facing': 'left'},
    '16:24': {'facing': 'down'},
    '24:33': {'facing': 'right'},
    '0': {'resting': true, 'moving': false, 'action': 'stand', 'speed_x': 0, 'speed_y': 0},
    '8': {'resting': true, 'moving': false, 'action': 'stand', 'speed_x': 0, 'speed_y': 0},
    '16': {'resting': true, 'moving': false, 'action': 'stand', 'speed_x': 0, 'speed_y': 0},
    '24': {'resting': true, 'moving': false, 'action': 'stand', 'speed_x': 0, 'speed_y': 0},
    '1:8': {'resting': false, 'moving': true, 'action': 'walk', 'speed_x': 0, 'speed_y': -10},
    '9:16': {'resting': false, 'moving': true, 'action': 'walk', 'speed_x': -10, 'speed_y': 0},
    '17:24': {'resting': false, 'moving': true, 'action': 'walk', 'speed_x': 0, 'speed_y': 10},
    '25:33': {'resting': false, 'moving': true, 'action': 'walk', 'speed_x': 10, 'speed_y': 0},
    'mode': 'update'
}"""

# bow tilesheets
properties_13x4 = """{
    ':': {'pose': '%P', 'skin': '%S', 'style': '%T', 'name': '%N', 'snap_to_rest': true, 'time_per_frame':100.0 },
    '0:13': {'facing': 'up'},
    '13:26': {'facing': 'left'},
    '26:39': {'facing': 'down'},
    '39:52': {'facing': 'right'},
    '0': {'resting': true, 'moving': false, 'action': 'stand', 'speed_x': 0, 'speed_y': 0},
    '13': {'resting': true, 'moving': false, 'action': 'stand', 'speed_x': 0, 'speed_y': 0},
    '26': {'resting': true, 'moving': false, 'action': 'stand', 'speed_x': 0, 'speed_y': 0},
    '39': {'resting': true, 'moving': false, 'action': 'stand', 'speed_x': 0, 'speed_y': 0},
    '1:13': {'resting': false, 'moving': true, 'action': 'walk', 'speed_x': 0, 'speed_y': -10},
    '14:26': {'resting': false, 'moving': true, 'action': 'walk', 'speed_x': -10, 'speed_y': 0},
    '27:39': {'resting': false, 'moving': true, 'action': 'walk', 'speed_x': 0, 'speed_y': 10},
    '40:52': {'resting': false, 'moving': true, 'action': 'walk', 'speed_x': 10, 'speed_y': 0},
    'mode': 'update'
}"""

# hurt tilesheets
properties_6x1 = """{
    ':': {'pose': '%P', 'skin': '%S', 'style': '%T', 'name': '%N', 'snap_to_rest': false, 'time_per_frame':200.0},
    '0:6': {'facing': 'down', 'resting': true, 'moving': true, 'action': 'walk'},
    'mode': 'update'
}"""

# combat dummy tilesheet - always moving
properties_8x1 = """{
    ':': {'pose': '%P', 'skin': '%S', 'style': '%T', 'name': '%N', 'snap_to_rest': false, 'time_per_frame':150.0},
    '0:8': {'facing': 'down', 'resting': true, 'moving': true, 'action': 'walk'},
    'mode': 'update'
}"""

# slash
properties_6x4 = """{
    ':': {'pose': '%P', 'skin': '%S', 'style': '%T', 'name': '%N', 'snap_to_rest': false, 'time_per_frame':150.0},
    '0': {'facing':'up', 'action': 'stand'},
    '6': {'facing':'left', 'action': 'stand'},
    '12': {'facing':'down', 'action': 'stand'},
    '18': {'facing':'right', 'action': 'stand'},
    '1:6': {'facing':'up', 'action': 'walk'},
    '7:12': {'facing':'left', 'action': 'walk'},
    '13:18': {'facing':'down', 'action': 'walk'},
    '19:': {'facing':'right', 'action': 'walk'},
    'mode': 'update'
    }"""

# spellcast
properties_7x4 = """{
    ':': {'pose': '%P', 'skin': '%S', 'style': '%T', 'name': '%N', 'snap_to_rest': false, 'time_per_frame':150.0},
    '0': {'facing':'up', 'action': 'stand'},
    '7': {'facing':'left', 'action': 'stand'},
    '14': {'facing':'down', 'action': 'stand'},
    '21': {'facing':'right', 'action': 'stand'},
    '1:7': {'facing':'up', 'action': 'walk'},
    '8:14': {'facing':'left', 'action': 'walk'},
    '15:21': {'facing':'down', 'action': 'walk'},
    '22:': {'facing':'right', 'action': 'walk'},
    'mode': 'update'
    }"""

defs = (
    # num_tiles_x, num_tiles_y, 'subdir', 'properties'
    (9, 4, 'walkcycle', properties_9x4),
    (8, 4, 'thrust', properties_8x4),
    (7, 4, 'spellcast', properties_7x4),
    (6, 4, 'slash', properties_6x4),
    (13, 4, 'bow', properties_13x4),
    (6, 1, 'hurt', properties_6x1),
    (8, 1, 'combat_dummy', properties_8x1),
)

re_spaces = re.compile('\s+')

# for num_tiles_x, num_tiles_y, subdir in defs:
#     assert os.access(subdir, os.F_OK)
#     for image_file in glob.glob('{}/*.png'.format(subdir)):
#         command = re_spaces.sub(' ', """
#             py -2 ../../../pytmxloader/spritesheet_mask_generator.py create_grid
#             --tile_width=64
#             --tile_height=64
#             {0} {1}
#             {2}.sdef2
#             --force
#             """.format(num_tiles_x, num_tiles_y, image_file))
#         os.system(command)

from pytmxloader import spritesheet_mask_generator as ssm_gen


# Program's stdout captured here for reference...
"""
pose         skin    style          name
----         ----    -----          ----
walkcycle    BEHIND  quiver         quiver
walkcycle    BELT    leather        leather
walkcycle    BELT    rope           rope
walkcycle    BODY    male           male
walkcycle    BODY    skeleton       skeleton
walkcycle    FEET    plate_armor    shoes
walkcycle    FEET    shoes          brown
walkcycle    HANDS   plate_armor    gloves
walkcycle    HEAD    chain_armor    helmet
walkcycle    HEAD    chain_armor    hood
walkcycle    HEAD    hair           blonde
walkcycle    HEAD    leather_armor  hat
walkcycle    HEAD    plate_armor    helmet
walkcycle    HEAD    robe           hood
walkcycle    LEGS    pants          greenish
walkcycle    LEGS    plate_armor    pants
walkcycle    LEGS    robe           skirt
walkcycle    TORSO   chain_armor    jacket_purple
walkcycle    TORSO   chain_armor    torso
walkcycle    TORSO   leather_armor  bracers
walkcycle    TORSO   leather_armor  shirt_white
walkcycle    TORSO   leather_armor  shoulders
walkcycle    TORSO   leather_armor  torso
walkcycle    TORSO   plate_armor    arms_shoulders
walkcycle    TORSO   plate_armor    torso
walkcycle    TORSO   robe_shirt     brown
walkcycle    WEAPON  shield         shield
walkcycle    WEAPON  shield         shield

pose         skin    style          name
----         ----    -----          ----
thrust       BEHIND  quiver         quiver
thrust       BELT    leather        leather
thrust       BELT    rope           rope
thrust       BODY    male           male
thrust       FEET    plate_armor    shoes
thrust       FEET    shoes          brown
thrust       HANDS   plate_armor    gloves
thrust       HEAD    chain_armor    helmet
thrust       HEAD    chain_armor    hood
thrust       HEAD    hair           blonde
thrust       HEAD    leather_armor  hat
thrust       HEAD    plate_armor    helmet
thrust       HEAD    robe           hood
thrust       LEGS    pants          greenish
thrust       LEGS    plate_armor    pants
thrust       LEGS    robe           skirt
thrust       TORSO   chain_armor    jacket_purple
thrust       TORSO   chain_armor    torso
thrust       TORSO   leather_armor  bracers
thrust       TORSO   leather_armor  shirt_white
thrust       TORSO   leather_armor  shoulders
thrust       TORSO   leather_armor  torso
thrust       TORSO   plate_armor    arms_shoulders
thrust       TORSO   plate_armor    torso
thrust       TORSO   robe_shirt     brown
thrust       WEAPON  shield         shield
thrust       WEAPON  shield         shield
thrust       WEAPON  spear          spear
thrust       WEAPON  staff          staff

pose         skin    style          name
----         ----    -----          ----
spellcast    BEHIND  quiver         quiver
spellcast    BELT    leather        leather
spellcast    BELT    rope           rope
spellcast    BODY    male           male
spellcast    BODY    skeleton       skeleton
spellcast    FEET    plate_armor    shoes
spellcast    FEET    shoes          brown
spellcast    HANDS   plate_armor    gloves
spellcast    HEAD    chain_armor    helmet
spellcast    HEAD    chain_armor    hood
spellcast    HEAD    hair           blonde
spellcast    HEAD    leather_armor  hat
spellcast    HEAD    plate_armor    helmet
spellcast    HEAD    robe           hood
spellcast    HEAD    skeleton       eyeglow
spellcast    LEGS    pants          greenish
spellcast    LEGS    plate_armor    pants
spellcast    LEGS    robe           skirt
spellcast    TORSO   chain_armor    jacket_purple
spellcast    TORSO   chain_armor    torso
spellcast    TORSO   leather_armor  bracers
spellcast    TORSO   leather_armor  shirt_white
spellcast    TORSO   leather_armor  shoulders
spellcast    TORSO   leather_armor  torso
spellcast    TORSO   plate_armor    arms_shoulders
spellcast    TORSO   plate_armor    torso
spellcast    TORSO   robe_shirt     brown

pose         skin    style          name
----         ----    -----          ----
slash        BEHIND  quiver         quiver
slash        BELT    leather        leather
slash        BELT    rope           rope
slash        BODY    male           male
slash        BODY    skeleton       skeleton
slash        FEET    plate_armor    shoes
slash        FEET    shoes          brown
slash        HANDS   plate_armor    gloves
slash        HEAD    chain_armor    helmet
slash        HEAD    chain_armor    hood
slash        HEAD    hair           blonde
slash        HEAD    leather_armor  hat
slash        HEAD    plate_armor    helmet
slash        HEAD    robe           hood
slash        LEGS    pants          greenish
slash        LEGS    plate_armor    pants
slash        LEGS    robe           skirt
slash        TORSO   chain_armor    jacket_purple
slash        TORSO   chain_armor    torso
slash        TORSO   leather_armor  bracers
slash        TORSO   leather_armor  shirt_white
slash        TORSO   leather_armor  shoulders
slash        TORSO   leather_armor  torso
slash        TORSO   plate_armor    arms_shoulders
slash        TORSO   plate_armor    torso
slash        TORSO   robe_shirt     brown
slash        WEAPON  dagger         dagger

pose         skin    style          name
----         ----    -----          ----
bow          BELT    leather        leather
bow          BELT    rope           rope
bow          BODY    male           male
bow          FEET    plate_armor    shoes
bow          FEET    shoes          brown
bow          HANDS   plate_armor    gloves
bow          HEAD    chain_armor    helmet
bow          HEAD    chain_armor    hood
bow          HEAD    hair           blonde
bow          HEAD    leather_armor  hat
bow          HEAD    plate_armor    helmet
bow          HEAD    robe           hood
bow          LEGS    pants          greenish
bow          LEGS    plate_armor    pants
bow          LEGS    robe           skirt
bow          TORSO   chain_armor    jacket_purple
bow          TORSO   chain_armor    torso
bow          TORSO   leather_armor  bracers
bow          TORSO   leather_armor  shirt_white
bow          TORSO   leather_armor  shoulders
bow          TORSO   leather_armor  torso
bow          TORSO   plate_armor    arms_shoulders
bow          TORSO   plate_armor    torso
bow          TORSO   robe_shirt     brown
bow          WEAPON  arrow          arrow
bow          WEAPON  bow            bow

pose         skin    style          name
----         ----    -----          ----
hurt         BEHIND  quiver         quiver
hurt         BELT    leather        leather
hurt         BELT    rope           rope
hurt         BODY    male           male
hurt         BODY    skeleton       skeleton
hurt         FEET    plate_armor    shoes
hurt         FEET    shoes          brown
hurt         HANDS   plate_armor    gloves
hurt         HEAD    chain_armor    helmet
hurt         HEAD    chain_armor    hood
hurt         HEAD    hair           blonde
hurt         HEAD    leather_armor  hat
hurt         HEAD    plate_armor    helmet
hurt         HEAD    robe           hood
hurt         LEGS    pants          greenish
hurt         LEGS    plate_armor    pants
hurt         LEGS    robe           skirt
hurt         TORSO   chain_armor    jacket_purple
hurt         TORSO   chain_armor    torso
hurt         TORSO   leather_armor  bracers
hurt         TORSO   leather_armor  shirt_white
hurt         TORSO   leather_armor  shoulders
hurt         TORSO   leather_armor  torso
hurt         TORSO   plate_armor    arms_shoulders
hurt         TORSO   plate_armor    torso
hurt         TORSO   robe_shirt     brown

pose         skin    style          name
----         ----    -----          ----
combat_dummy BODY    male           male
"""

count = 0
debug = False
for num_tiles_x, num_tiles_y, subdir, properties in defs:
    assert os.access(subdir, os.F_OK)
    print('')
    print('{:12s} {:7s} {:14s} {}'.format('pose', 'skin', 'style', 'name'))
    print('{:12s} {:7s} {:14s} {}'.format('----', '----', '-----', '----'))
    for image_file in glob.glob('{}/*.png'.format(subdir)):
        # print(image_file)
        filepart = os.path.splitext(os.path.split(image_file)[1])[0]
        parts = filepart.split('_')
        # print(' '.join(parts))
        # "':': {'pose': '%P', 'skin': '%S', 'style': '%T', 'name': '%N'},"
        pose = subdir
        skin = parts.pop(0)
        style = parts.pop(0)
        if skin in ('HEAD', 'LEGS', 'FEET'):
            if parts[0] == 'armor':
                style += '_' + parts.pop(0)
            name = ' '.join(parts)
        elif skin in ('BODY', 'BEHIND', 'WEAPON', 'BELT'):
            name = style
        else:
            if style in ('leather chain plate robe'):  # TODO: ask Gumm if that is correct (iterating string!?)
                style += '_' + parts.pop(0)
            if parts:
                name = '_'.join(parts)
            else:
                name = style
        if debug:
            print('')
            print('{:12s} {:7s} {:14s} {}'.format(pose, skin, style, name))
        props = properties
        props = props.replace('%P', pose)
        props = props.replace('%S', skin)
        props = props.replace('%T', style)
        props = props.replace('%N', name)
        props = props.replace(" ", "")
        props = ''.join(props.splitlines())
        props = props.replace("'", '"')
        s = json.loads(props)
        print('{:12s} {:7s} {:14s} {}'.format(s[':']['pose'], s[':']['skin'], s[':']['style'], s[':']['name']))
        command = re_spaces.sub(' ', """ --properties={props}
            --force
            create_grid
            --tile_width=64
            --tile_height=64
            {nx} {ny}
            {file}.sdef9
            """.format(props=props, nx=num_tiles_x, ny=num_tiles_y, file=image_file))
        ssm_gen.main(command.split())
