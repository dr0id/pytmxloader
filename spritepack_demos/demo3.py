# coding=utf-8

import glob
import sys
sys.path.insert(0, '..')

import pygame
from pygame.locals import *

from pytmxloader.spritesheetlib import FileInfo, SpritesheetLib10, DummyLogger

button_font = None


def load_poses():
    poses = 'walkcycle thrust spellcast slash hurt combat_dummy bow'.split(' ')
    spritesheetlib = SpritesheetLib10(DummyLogger())
    result = {}
    for subdir in poses:
        anims = []
        for filename in glob.glob('{}/*.sdef9'.format(subdir)):
            sprites = spritesheetlib.load_spritesheet(FileInfo(filename))
            try:
                # sprite_sheet_dict = sprites.get_grouped_by_facing_and_action()
                sprite_sheet_dict = sprites.get_grouped_by_action_and_facing()
                anim = Animation(sprite_sheet_dict, name=filename)
                anims.append(anim)
            except Exception as ex:
                print("Error", subdir, filename, ex)
                pass
        result[subdir] = anims
    # print('found:')
    # print(result)
    return result


class Animation(object):

    def __init__(self, sprite_sheet, name=""):
        self.sprites = sprite_sheet
        self.action = self.sprites.keys()[0]
        self.facing = self.sprites[self.action].keys()[0]
        self.image = pygame.Surface((0, 0))
        self.index = 0
        self.count = 0
        self.elapsed = 0
        self.time_per_frame = 0.010  # seconds
        self.name = name
        # facing = sprite_sheet.keys()[0]
        # self.set_facing(facing)
        # self.set_action(sprite_sheet[facing].keys()[0])
        self.reset()

    def reset(self):
        self.elapsed = 0
        self.index = 0
        # action = self.sprites.keys()[0]
        # self.set_action(action)
        # self.set_facing(self.sprites[self.action].keys()[0])

        sprite = self.sprites[self.action][self.facing][self.index]
        self.count = len(self.sprites[self.action][self.facing])

        self.time_per_frame = sprite.properties['time_per_frame'] / 1000.0  # convert from ms to seconds
        self.image = sprite.image

    def _update_index(self):
        # self.count = len(self.sprites[self.facing][self.action])
        self.count = len(self.sprites[self.action][self.facing])
        if self.index >= self.count:
            self.index = 0
            # self.image = self.sprites[self.facing][self.action][self.index].image
            self.image = self.sprites[self.action][self.facing][self.index].image

    def set_facing(self, new_facing):
        # assert new_facing in self.sprites, 'new facing unknown: ' + str(new_facing)
        # assert self.action in self.sprites[new_facing], 'action in new facing unknown: ' + str(self.action)
        # assert new_facing in self.sprites[self.action]
        if new_facing in self.sprites[self.action]:
            self.facing = new_facing
            self._update_index()

    def set_action(self, new_action):
        # assert new_action in self.sprites[self.facing]
        assert new_action in self.sprites
        assert self.facing in self.sprites[new_action]
        self.action = new_action
        self._update_index()

    def update(self, dt):

        self.elapsed += dt

        # skip frames (will hopefully never happen)
        frames = 0
        while self.elapsed >= self.time_per_frame:
            self.elapsed -= self.time_per_frame  # this would be a nicer animation, but makes animations go out of sync
            frames += 1
            self.elapsed = 0
            break  # just keep it simple and synchronized with other animations

        if frames > 0:
            self.index += frames
            if self.index >= self.count:
                # end of animation
                pass
            self.index %= self.count

            # sprite = self.sprites[self.facing][self.action][self.index]
            sprite = self.sprites[self.action][self.facing][self.index]
            self.time_per_frame = sprite.properties['time_per_frame'] / 1000.0  # convert from ms to seconds
            self.image = sprite.image


class ButtonGroup(pygame.sprite.OrderedUpdates):

    def __init__(self, radio=True, *sprites):
        pygame.sprite.OrderedUpdates.__init__(self, *sprites)
        self.radio = radio

    def mouse_click(self, pos):
        clicked = None
        for s in self:
            if s.rect.collidepoint(pos):
                clicked = s
                break
        if clicked:
            if self.radio:
                for s in self:
                    s.off()
            clicked.on_click()


class Button(pygame.sprite.Sprite):

    def __init__(self, name, callbacks, surface=None, *groups):
        pygame.sprite.Sprite.__init__(self, *groups)
        self.name = name
        # "off" button
        text = button_font.render(name, True, Color('white'))
        text_rect = text.get_rect()
        if surface:
            self.image = surface.copy()
        else:
            self.image = pygame.Surface(text_rect.inflate(4, 4).size)
            surface = self.image.copy()
        self.rect = self.image.get_rect()
        text_rect.center = self.image.get_rect().center
        self.image.blit(text, text_rect)
        self.image_off = self.image
        # "on" button
        self.image_on = surface.copy()
        text_fringe = pygame.Surface(text.get_size())
        text_fringe.blit(text, (0, 0))
        text_fringe.set_alpha(192)
        for x in (-1, 0, 1):
            for y in (-1, 0, 1):
                self.image_on.blit(text_fringe, text_rect.move(x, y))
        self.image_on.blit(text, text_rect)
        self.callbacks = list(callbacks)

    def on_click(self):
        for cb in self.callbacks:
            cb(self)


class ToggleButton(Button):

    def __init__(self, name, callbacks, surface=None, *groups):
        Button.__init__(self, name, callbacks, surface, *groups)
        self.is_on = False

    def on(self):
        self.image = self.image_on
        self.is_on = True

    def off(self):
        self.image = self.image_off
        self.is_on = False

    def on_click(self):
        if self.is_on:
            self.off()
        else:
            self.on()
        for cb in self.callbacks:
            # check the state in the callback!
            cb(self)


def make_button_bank(pos, selected_idx, buttons, direction=1, radio=True):
    group = ButtonGroup(radio=radio)
    x, y = pos
    for button in buttons:
        if direction:
            # vertical
            button.rect.topleft = x, y
            y += button.rect.h + 2
        else:
            button.rect.topleft = x, y
            x += button.rect.w + 2
        group.add(button)

    if buttons and selected_idx >= 0 and radio:
        group.sprites()[selected_idx].on()
    return group


def do_switch(sw_btn):
    idx, btns, active_anims = sw_btn.tag
    b1 = btns[idx]
    b2 = btns[idx+1]
    tl = b1.rect.topleft
    b1.rect.topleft = b2.rect.topleft
    b2.rect.topleft = tl
    btns[idx] = b2
    btns[idx+1] = b1
    b1.index, b2.index = b2.index, b1.index

    a1 = active_anims[idx]
    active_anims[idx] = active_anims[idx + 1]
    active_anims[idx + 1] = a1


def print_click(btn):
    print(btn.name, btn.is_on, btn.index, btn.tag)


def activate_animation(btn):
    anim, cur_anims = btn.tag
    for a in cur_anims:
        if a:
            anim.set_action(a.action)
            anim.set_facing(a.facing)
            break
    if btn.is_on:
        cur_anims[btn.index] = anim
    else:
        cur_anims[btn.index] = None
    for a in cur_anims:
        if a:
            a.reset()


def change_action(btn):
    anims, action = btn.tag
    for anim in anims:
        anim.set_action(action)


def create_buttons(animations, active_animations):
    px = 40
    py = 40
    btns = []
    for i, animation in enumerate(animations):
        b = ToggleButton(animation.name, [activate_animation, print_click])
        b.index = i
        b.tag = [animation, active_animations]
        btns.append(b)
    group = make_button_bank((px, py), 0, btns, radio=False)
    switchers = []
    for i in range(len(animations) - 1):
        b = Button('(', [do_switch])
        b.index = i
        b.tag = (i, btns, active_animations)
        switchers.append(b)
    sw_group = make_button_bank((px - 20, py + 10), 0, switchers, radio=False)
    chains = []
    # for i in range(count - 1):
    # b = ToggleButton('[', [])
    #     b.tag = (i, btns)
    #     chains.append(b)
    ch_group = make_button_bank((px - 10, py + 10), 0, chains, radio=False)
    actions = []
    a_btns = ButtonGroup()
    try:
        if animations:
            for a in animations[0].sprites.keys():
                b = ToggleButton(str(a), [change_action])
                b.tag = animations, a
                actions.append(b)
        a_btns = make_button_bank((px + 800, py + 20), 0, actions)
    except Exception as ex:
        print(ex)
    return [ch_group, group, sw_group, a_btns]


def activate_pose(btn):
    poses, animations, active_animations, groups = btn.tag
    del groups[1:]
    del active_animations[:]
    del animations[:]
    animations.extend(poses[btn.name])
    print('activating pose: {0} containing animations: {1}'.format(btn.name, [(a.name, a.sprites) for a in animations]))
    active_animations.extend([None] * len(animations))
    groups.extend(create_buttons(animations, active_animations))
    print(btn.name, btn.tag)


def main():
    global button_font

    pygame.init()
    resolution = 1024, 768
    screen = pygame.display.set_mode(resolution)
    screen_rect = screen.get_rect()
    bg_color = Color('darkgreen')
    button_font = pygame.font.SysFont('sans', 14)

    clock = pygame.time.Clock()
    max_fps = 30
    dt = 1.0 / max_fps

    poses = load_poses()
    animations = []
    active_animations = [None] * len(animations)
    groups = []

    pose_btns = []
    for pose in sorted(poses.keys()):
        b = ToggleButton(pose, [activate_pose])
        b.tag = poses, animations, active_animations, groups
        pose_btns.append(b)
    pose_group = make_button_bank((10, 10), 0, pose_btns, 0)

    groups.append(pose_group)
    groups.extend(create_buttons(animations, active_animations))

    running = True
    while running:
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                # for anim in active_animations:
                #     if anim:
                #         anim.set_action('walk')
                if e.key == K_UP:
                    for anim in active_animations:
                        if anim:
                            anim.set_facing('up')
                elif e.key == K_DOWN:
                    for anim in active_animations:
                        if anim:
                            anim.set_facing('down')
                elif e.key == K_RIGHT:
                    for anim in active_animations:
                        if anim:
                            anim.set_facing('right')
                elif e.key == K_LEFT:
                    for anim in active_animations:
                        if anim:
                            anim.set_facing('left')
                elif e.key == K_ESCAPE:
                    # run away!
                    running = False
            # elif e.type == KEYUP:
            #     for anim in active_animations:
            #         if anim:
            #             anim.set_action('stand')
            elif e.type == MOUSEBUTTONDOWN:
                # check the gui
                for g in groups:
                    g.mouse_click(e.pos)
            elif e.type == QUIT:
                running = False

        # update animations
        clock.tick(max_fps)
        for anim in active_animations:
            if anim:
                anim.update(dt)

        screen.fill(bg_color)

        # draw animations
        for anim in active_animations:
            if anim:
                image = anim.image
                rect = image.get_rect(center=screen_rect.center)
                screen.blit(image, rect)

        # draw gui (buttons)
        for g in groups:
            g.draw(screen)
        pygame.display.flip()


if __name__ == '__main__':
    main()
