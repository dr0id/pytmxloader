import glob
import sys
sys.path.insert(0, '..')

import pygame
from pygame.locals import *

from pytmxloader.spritesheetlib import FileInfo, SpritesheetLib10, DummyLogger


class Anim(object):

    def __init__(self, base_frames, speed=0.0, anchor='center'):
        self.base_frames = [s.image for s in base_frames]
        self.speed = float(speed) / len(self.base_frames)
        self.framei = 0
        self.elapsed = 0.0
        self.armor = []
        self.anchor = anchor

    def start(self):
        self.framei = 0
        self.elapsed = 0.0

    def update(self, dt):
        self.elapsed += dt
        if self.elapsed >= self.speed:
            self.elapsed -= self.speed
            self.framei += 1
            if self.framei == len(self.base_frames):
                self.framei = 0

    def render(self, surface, pos):
        i = self.framei
        layers = [self.base_frames[i]]
        layers.extend([a[i].image for a in self.armor])
        for image in layers:
            rect = image.get_rect()
            setattr(rect, self.anchor, pos)
            surface.blit(image, rect)


class Still(Anim):
    # Just one frame, no motion

    def start(self):
        pass

    def update(self, dt):
        pass


def main():
    pygame.init()

    resolution = 400, 200
    screen = pygame.display.set_mode(resolution)
    screen_rect = screen.get_rect()
    bg_color = Color('darkgreen')

    clock = pygame.time.Clock()
    max_fps = 30
    dt = max_fps / 1000.0

    def add_armor(anim, mode, armor, face):
        print('add_armor: mode={}, armor={}, face={}'.format(
            'standing' if mode == 'standing' else 'walking', armor, face))
        del anim.armor[:]
        if armor is None:
            return
        for struct in mode[armor]:
            anim.armor.append(struct[face])

    def make_anim(mode, armor, face):
        print('make_anim: mode={}, armor={}, face={}'.format(
            'standing' if mode == 'standing' else 'walking', armor, face))
        if mode is standing:
            anim = Still(mode['body'][0][face])
        else:
            anim = Anim(mode['body'][0][face], 0.5)
        add_armor(anim, mode, armor, face)
        return anim

    # Get the the many layers of sprite sheets.
    globs = dict(
        body=glob.glob('walkcycle/*body*sdef'),
        leather=glob.glob('walkcycle/*leather*sdef'),
        chain=glob.glob('walkcycle/*chain*sdef'),
        plate=glob.glob('walkcycle/*plate*sdef'),
    )
    spritesheet = SpritesheetLib10(DummyLogger())
    # standing = {
    #   'body':    [{'up': [sprites...], 'down': [sprites...], ...],
    #   'leather': [{'up': [sprites...], 'down': [sprites...], ...],
    #   'chain':   [{'up': [sprites...], 'down': [sprites...], ...],
    #   'plate':   [{'up': [sprites...], 'down': [sprites...], ...],
    # }
    # walking = {same structure, only one sprite per face...}
    standing = {}
    walking = {}
    for armor_name, file_list in globs.items():
        for filename in file_list:
            file_info = FileInfo(filename)
            sprites = spritesheet.load_spritesheet(file_info)
            # make a 2D array for easier indexing
            sprite_rows = []
            for i in range(4):
                o = i * 9
                sprite_rows.append(sprites[o:o + 9])
            if armor_name not in standing:
                standing[armor_name] = []
            if armor_name not in walking:
                walking[armor_name] = []
            # organize the sprites for lookup
            standing[armor_name].append(
                dict(
                    up=sprite_rows[0][0:1],
                    left=sprite_rows[1][0:1],
                    down=sprite_rows[2][0:1],
                    right=sprite_rows[3][0:1],
                ))
            walking[armor_name].append(
                dict(
                    up=sprite_rows[0][1:],
                    left=sprite_rows[1][1:],
                    down=sprite_rows[2][1:],
                    right=sprite_rows[3][1:],
                ))
    # keys stack and current anim
    keys_down = []
    current_mode = standing     # or walking
    current_armor = None        # or leather, chain, plate
    current_face = 'down'       # or up, left, right
    # anim is updated and rendered
    current_anim = make_anim(current_mode, current_armor, current_face)
    # a circular queue for the armor switcher
    next_armor = ['leather', 'chain', 'plate', None]

    pygame.display.set_caption(str(current_armor))

    running = True
    while running:
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                if e.key in (K_UP, K_LEFT, K_DOWN, K_RIGHT):
                    # set up the walking anim
                    keys_down.append(e.key)
                    current_mode = walking
                    current_face = pygame.key.name(e.key)
                    current_anim = make_anim(current_mode, current_armor, current_face)
                    current_anim.start()
                elif e.key == K_SPACE:
                    # switch armor
                    current_armor = next_armor.pop(0)
                    next_armor.append(current_armor)
                    add_armor(current_anim, current_mode, current_armor, current_face)
                    pygame.display.set_caption(str(current_armor))
                elif e.key == K_ESCAPE:
                    # run away!
                    running = False
            elif e.type == KEYUP:
                if e.key in (K_UP, K_LEFT, K_DOWN, K_RIGHT):
                    # return to standing, or to previous anim if more than one key was pressed
                    if e.key in keys_down:
                        keys_down.remove(e.key)
                        if keys_down:
                            # keys pressed: we're still walking
                            current_mode = walking
                            current_face = pygame.key.name(keys_down[-1])
                        else:
                            # no keys pressed: standing still
                            current_mode = standing
                            current_face = pygame.key.name(e.key)
                        current_anim = make_anim(current_mode, current_armor, current_face)
            elif e.type == QUIT:
                running = False

        clock.tick(max_fps)
        current_anim.update(dt)

        screen.fill(bg_color)
        current_anim.render(screen, screen_rect.center)
        pygame.display.flip()


if __name__ == '__main__':
    main()
