from collections import namedtuple
import glob
import sys
sys.path.insert(0, '..')

import pygame
from pygame.locals import *

from pytmxloader.spritesheetlib import FileInfo, SpritesheetLib10, DummyLogger


SpriteSheetInfo = namedtuple('SpriteSheetInfo', 'sdef image')

button_font = None


class SpritesheetHelper(object):

    POSES = 'walkcycle thrust spellcast slash hurt combat_dummy bow'.split(' ')

    def __init__(self):
        self.spritesheetlib = SpritesheetLib10(DummyLogger())
        self.file_info_bucket = {}
        for subdir in self.POSES:
            for filename in glob.glob('{}/*.sdef'.format(subdir)):
                file_info = FileInfo(filename)
                self.file_info_bucket[file_info.full_name.lower()] = file_info
        self.sheets_map = {}

    def get_sheets(self, pose, *words):
        sheets_map = {}
        for key, file_info in self.file_info_bucket.items():
            if pose in key and all([word in key for word in words]):
                sdef = self.spritesheetlib.load_sdef_from_file(file_info)
                image = pygame.image.load(file_info.full_name.replace(".sdef", ""))
                sheets_map[key] = SpriteSheetInfo(sdef=sdef, image=image)
        return sheets_map

    def get_sprites(self, sdef, surface):
        return self.spritesheetlib.load_spritesheet_from_sdef_and_surface(sdef, surface)
spritesheet_helper = SpritesheetHelper()


class Anim(object):

    FACING = {'up': 0, 'left': 1, 'down': 2, 'right': 3}

    def __init__(self, pose, *terms, **kwargs):
        moving = kwargs.get('moving', False)
        facing = kwargs.get('facing', 'down')
        speed = kwargs.get('speed', 1.0)
        self.image = None           # The current frame's image
        self.body_type = None       # Body type human or skeleton
        self.pose = pose            # The name of the pose, one of SpriteSheet.POSES
        self.sheet = None           # The entire spritesheet surface
        self.sdef = None            # The sprite definition (json dict) for this spritesheet
        self.moving = moving        # Boolean: is/not moving
        self.facing = facing        # Facing direction, one of self.FACING
        self.speed = speed          # One anim sequence is played over this timespan
        self.elapsed = 0.0          # Measure passage of time
        self.frame_sequence = []    # Sequence of frame indices to play

        # Pretty loose organization. We just search the list of filenames for the words in *terms. We can do this
        # because the sprite sheets are named in an orderly fashion. If there is no file matching a term it is ignored:
        # so missing files are not detected.
        for words in terms:
            # Get the entire surface of the sheet. Keep the first one and layer subesequent ones over it.
            for sdef, image in spritesheet_helper.get_sheets(self.pose, *words).values():
                if self.sheet is None:
                    self.sheet = image
                    self.body_type = words[0]
                    self.sdef = sdef
                else:
                    self.sheet.blit(image, (0, 0))

        # Build still and moving frames using the new SpritesList.get_rows() and SpritesList.get_cols() methods.
        self.sprites = spritesheet_helper.get_sprites(self.sdef, self.sheet)
        facing = 'up', 'left', 'down', 'right'
        if self.pose in ('combat_dummy', 'hurt'):
            self.still_frames = dict(zip(facing, [self.sprites.get_rows()[0]] * 4))
            self.moving_frames = self.still_frames
        else:
            self.still_frames = dict(zip(facing, [[col] for col in self.sprites.get_columns()[0]]))
            self.moving_frames = dict(zip(facing, [sprite_row[1:] for sprite_row in self.sprites.get_rows()]))
        #
        #  # A more verbose way that does not use SpritesList.get_cols(). Perhaps a little easier to understand.
        # sprite_rows = self.sprites.get_rows()
        # self.still_frames = {}
        # self.moving_frames = {}
        # if self.pose in ('combat_dummy', 'hurt'):
        #     for facing in self.FACING:
        #         self.still_frames[facing] = sprite_rows[0]
        #         self.moving_frames = self.still_frames
        # else:
        #     for facing in self.FACING:
        #         rowi = self.FACING[facing]
        #         self.still_frames[facing] = sprite_rows[rowi][0:1]
        #         self.moving_frames[facing] = sprite_rows[rowi][1:]

    def face(self, way):
        """valid ways are 'up', 'down', 'left', 'right'"""
        assert way in self.FACING
        self.facing = way

    def update(self, dt):
        """progress the frame index and set the current image"""
        # self.frame_sequence is a list of indices that represent an animation cycle. Animation cycles are always played
        # to the end by consuming the frame sequence (pop(0)) as time passes. When the sequence is exhausted either the
        # anim cycle is restarted (if self.moving) or the still frame is selected.

        # Consume the current frame sequence when the time has elapsed. Timing is calculated as one complete sequence
        # per self.speed. This happens to work okay for all poses, but the hurt pose is a little slow-motion at that
        # speed so we'll play it much faster. See also the "elif self.moving" block below where the sequence is tweaked
        # even more for the hurt pose.
        if self.frame_sequence:
            self.elapsed += dt
            frames = self.moving_frames[self.facing]
            hurt = 8 if self.pose == 'hurt' else 1
            time_per_frame = float(self.speed) / len(frames) / hurt
            if self.elapsed >= time_per_frame:
                self.elapsed -= time_per_frame
                self.frame_sequence.pop(0)

        if self.frame_sequence:
            # continue the frame sequence
            frames = self.moving_frames[self.facing]
            self.image = frames[self.frame_sequence[0]].image
        elif self.moving:
            # restart the frame sequence
            frames = self.moving_frames[self.facing]
            self.frame_sequence = range(len(frames))
            # If this is the hurt pose: add 4 "stunned flat" frames, and then reverse and lengthen the "fall down"
            # frames to make him stand back up slower.
            if self.pose == 'hurt':
                self.frame_sequence += self.frame_sequence[-1:] * 4
                for i in sorted(self.frame_sequence, reverse=True):
                    self.frame_sequence.extend((i, i, i, i))
            self.image = frames[0].image
        else:
            # select the still frame
            frames = self.still_frames[self.facing]
            self.image = frames[0].image


class ButtonGroup(pygame.sprite.OrderedUpdates):

    def __init__(self, *sprites):
        pygame.sprite.OrderedUpdates.__init__(self, *sprites)

    def mouse_click(self, pos):
        clicked = None
        for s in self:
            if s.rect.collidepoint(pos):
                clicked = s
                break
        if clicked:
            for s in self:
                if s is clicked:
                    s.on()
                else:
                    s.off()
        return clicked


class Button(pygame.sprite.Sprite):

    def __init__(self, name, surface, *groups):
        pygame.sprite.Sprite.__init__(self, *groups)
        self.name = name
        # "off" button
        self.image = surface.copy()
        self.rect = self.image.get_rect()
        text = button_font.render(name, True, Color('white'))
        text_rect = text.get_rect(center=self.rect.center)
        self.image.blit(text, text_rect)
        self.image_off = self.image
        # "on" button
        self.image_on = surface.copy()
        #self.image_on.blit(text, text_rect)
        text_fringe = pygame.Surface(text.get_size())
        text_fringe.blit(text, (0, 0))
        text_fringe.set_alpha(192)
        for x in (-1, 0, 1):
            for y in (-1, 0, 1):
                self.image_on.blit(text_fringe, text_rect.move(x, y))
        self.image_on.blit(text, text_rect)

    def on(self):
        self.image = self.image_on

    def off(self):
        self.image = self.image_off


def make_button_bank(pos, select, *names):
    group = ButtonGroup()
    max_width = 0
    max_height = 0
    for name in names:
        size = button_font.size(name)
        max_width, max_height = max(max_width, size[0]), size[1]
    surface = pygame.Surface((max_width + 4, max_height + 4))
    rect = surface.get_rect()
    pygame.draw.rect(surface, Color('skyblue'), rect, 1)
    x, y = pos
    for name in names:
        button = Button(name, surface, group)
        button.rect.topleft = x, y
        y += button.rect.h + 2
    if select >= 0:
        group.sprites()[select].on()
    return group


def click_bank(buttons, pos, val):
    button = buttons.mouse_click(pos)
    if button:
        return button.name
    else:
        return val


def new_anim(pose, body_type, armor_set, **kwargs):
    layers = [body_type]
    if armor_set == 'leather':
        layers.append(('pants_greenish', 'legs'))
    else:
        layers.append((armor_set, 'legs'))
    layers.extend([(armor_set, piece) for piece in 'torso shirt_white shoulders bracers hat helmet hood'.split(' ')])
    if pose == 'thrust':
        print('adding thrust weapons')
        layers.insert(0, ('weapon', 'shield'))
        layers.append(('weapon', 'spear'))
    elif pose == 'slash':
        print('adding slash weapon')
        layers.append(('weapon', 'dagger'))
    elif pose == 'bow':
        print('adding bow weapons')
        layers.extend([('weapon', 'bow'), ('weapon', 'arrow')])
    elif pose == 'combat_dummy':
        print('special combat dummy')
        layers = ['male']
    return Anim(pose, *layers, **kwargs)


def main():
    global button_font

    pygame.init()
    resolution = 800, 600
    screen = pygame.display.set_mode(resolution)
    screen_rect = screen.get_rect()
    bg_color = Color('darkgreen')
    button_font = pygame.font.SysFont('sans', 14)

    clock = pygame.time.Clock()
    max_fps = 30
    dt = 1.0 / max_fps

    # keys stack and current anim
    keys_down = []
    poses = list(spritesheet_helper.POSES)
    current_type = 'male'
    current_pose = poses[0]
    current_armor_set = 'leather'
    current_face = 'down'       # or up, left, right

    # starting anim
    anim = new_anim(current_pose, ('body', current_type), current_armor_set, facing=current_face, speed=1.0)

    # gui button banks for changing the spritesheets
    body_buttons = make_button_bank((10, 10), 0, 'male', 'skeleton')
    armor_set_buttons = make_button_bank((10, 60), 0, 'leather', 'chain', 'plate', 'robe')
    pose_buttons = make_button_bank(
        (10, 155), 0, 'walkcycle', 'thrust', 'spellcast', 'slash', 'hurt', 'combat_dummy', 'bow')

    running = True
    while running:
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                if e.key in (K_UP, K_LEFT, K_DOWN, K_RIGHT):
                    # set up the walking anim
                    keys_down.append(e.key)
                    anim.moving = True
                    current_face = pygame.key.name(e.key)
                    anim.facing = current_face
                elif e.key == K_ESCAPE:
                    # run away!
                    running = False
            elif e.type == KEYUP:
                if e.key in (K_UP, K_LEFT, K_DOWN, K_RIGHT):
                    # return to standing, or to previous anim if more than one key was pressed
                    if e.key in keys_down:
                        keys_down.remove(e.key)
                    if keys_down:
                        current_face = pygame.key.name(keys_down[-1])
                        anim = new_anim(
                            current_pose, ('body', current_type), current_armor_set,
                            moving=True, facing=current_face, speed=1.0)
                    else:
                        anim.moving = False
            elif e.type == MOUSEBUTTONDOWN:
                # check the gui
                pre = current_type, current_pose, current_armor_set
                current_type = click_bank(body_buttons, e.pos, current_type)
                current_pose = click_bank(pose_buttons, e.pos, current_pose)
                current_armor_set = click_bank(armor_set_buttons, e.pos, current_armor_set)
                if pre != (current_type, current_pose, current_armor_set):
                    anim = new_anim(
                        current_pose, ('body', current_type), current_armor_set, facing=current_face, speed=1.0)
            elif e.type == QUIT:
                running = False

        clock.tick(max_fps)
        anim.update(dt)

        screen.fill(bg_color)
        image = anim.image
        rect = image.get_rect(center=screen_rect.center)
        screen.blit(image, rect)
        body_buttons.draw(screen)
        armor_set_buttons.draw(screen)
        pose_buttons.draw(screen)
        pygame.display.flip()


if __name__ == '__main__':
    main()
