===========
pytmxloader
===========

.. .. image:: https://badge.fury.io/py/pytmxloader.png
    :target: http://badge.fury.io/py/pytmxloader
    
.. .. image:: https://travis-ci.org/dr0id/pytmxloader.png?branch=master
        :target: https://travis-ci.org/dr0id/pytmxloader

.. .. image:: https://pypip.in/d/pytmxloader/badge.png
        :target: https://crate.io/packages/pytmxloader?version=latest


pytmxloader is intended to make loading of json files of tiled_ very easy.

* Free software: BSD license
.. * Documentation: http://pytmxloader.rtfd.org.
.. * Discussion: https://dr0id.bitbucket.org/TODO


Features
--------

* simple loading of the json files produced by the tmx map editor tiled_
* import the data into object data structures ready to be used to load the resources
* pre-calculated positions of objects in pixels (may or may not match your screen coordinates)


.. _tiled: http://www.mapeditor.org/
