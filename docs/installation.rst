============
Installation
============

At the command line::

    $ easy_install pytmxloader

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv pytmxloader
    $ pip install pytmxloader

