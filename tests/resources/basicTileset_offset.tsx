<?xml version="1.0" encoding="UTF-8"?>
<tileset name="basicTilesetOffset" tilewidth="32" tileheight="32" margin="5" tilecount="4">
 <tileoffset x="3" y="4"/>
 <image source="basicTileset.png" width="138" height="42"/>
 <tile id="0" probability="0.5"/>
 <tile id="2" probability="0.5"/>
</tileset>
