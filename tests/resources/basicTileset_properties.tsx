<?xml version="1.0" encoding="UTF-8"?>
<tileset name="basicTilesetProperties" tilewidth="32" tileheight="32" margin="5" tilecount="4">
 <properties>
  <property name="p4" value="p44"/>
 </properties>
 <image source="basicTileset.png" width="138" height="42"/>
 <tile id="0">
  <properties>
   <property name="p3" value="p33"/>
  </properties>
 </tile>
</tileset>
