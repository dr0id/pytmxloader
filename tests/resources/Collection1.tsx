<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Collection1" tilewidth="256" tileheight="160" tilecount="2">
 <properties>
  <property name="im_col_prop1" value="im_col_prop1_value"/>
 </properties>
 <tile id="0">
  <properties>
   <property name="tile_prop2" value="tile_prop2_value"/>
  </properties>
  <image width="138" height="42" source="../basicTileset.png"/>
 </tile>
 <tile id="1">
  <image width="256" height="160" source="tiles01_tiles8x5_size32x32_m0_s0.png"/>
 </tile>
</tileset>
