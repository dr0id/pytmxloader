<?xml version="1.0" encoding="UTF-8"?>
<tileset name="basicTilesetAnimation" tilewidth="32" tileheight="32" margin="5" tilecount="4">
 <image source="basicTileset.png" width="138" height="42"/>
 <tile id="2" probability="0.75">
  <properties>
   <property name="p9" value="p99"/>
  </properties>
  <animation>
   <frame tileid="1" duration="500"/>
   <frame tileid="3" duration="250"/>
  </animation>
 </tile>
</tileset>
