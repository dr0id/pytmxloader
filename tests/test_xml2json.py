#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pytmxloader
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""

import json
import unittest
import datadriventestingdecorators as ddtd

from pytmxloader import xml2json as mut  # module under test

__version__ = '4.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = 'DR0ID'
__email__ = 'dr0iddr0id [at] googlemail [dot] com'
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


# noinspection SpellCheckingInspection
@ddtd.use_data_driven_testing_decorators
class TestPytmxloader(unittest.TestCase):
    def setUp(self):
        pass

    @ddtd.test_case({'a': 1, 'b': 2}, {'a': 1, 'b': 2})
    @ddtd.test_case({'b': 2, 'a': 1}, {'a': 1, 'b': 2}, test_name="different_order_in_dicts")
    @ddtd.test_case({'a': 1, 'b': [1, 2, 3]}, {'a': 1, 'b': [1, 2, 3]})
    @ddtd.test_case({'a': 1, 'b': {'a': 1, 'b': 2}}, {'a': 1, 'b': {'a': 1, 'b': 2}})
    @ddtd.test_case({"height": 66, "layers": [
        {"data": [22, 22, 23], "height": 66, "name": "Layer 0", "opacity": 1, "type": "tilelayer", "visible": True,
         "width": 102, "x": 0, "y": 0}], "orientation": "orthogonal", "properties": {}, "tileheight": 28, "tilesets": [
        {"firstgid": 1, "image": "minix.png", "imageheight": 364, "imagewidth": 240, "margin": 0, "name": "mini2x",
         "properties": {}, "spacing": 0, "tileheight": 28, "tileproperties": {"84": {"walkable": "0", "water": "1"}},
         "tilewidth": 24}], "tilewidth": 24, "version": 1, "width": 102},
        {"height": 66, "layers": [
            {"data": [22, 22, 23], "height": 66, "name": "Layer 0", "opacity": 1, "type": "tilelayer",
             "visible": True, "width": 102, "x": 0, "y": 0}], "orientation": "orthogonal", "properties": {},
         "tileheight": 28, "tilesets": [
            {"firstgid": 1, "image": "minix.png", "imageheight": 364, "imagewidth": 240, "margin": 0,
             "name": "mini2x", "properties": {}, "spacing": 0, "tileheight": 28,
             "tileproperties": {"84": {"walkable": "0", "water": "1"}}, "tilewidth": 24}], "tilewidth": 24,
         "version": 1, "width": 102})
    def test_same_dicts(self, expected, actual):
        # arrange
        # act
        # verify
        self.assertDictEqual(expected, actual)

    @ddtd.test_case("./resources/map_tileQT09_v10_orthogonal_10x10tiles_32x32tilesize_0layers_0tilesets_bgFF00FF.tmx",
                    "./resources/map_tileQT09_v10_orthogonal_10x10tiles_32x32tilesize_0layers_0tilesets_bgFF00FF.json")
    @ddtd.test_case("./resources/map_tileQT09_v10_staggert_10x10tiles_32x32tilesize_0layers_0tilesets.tmx",
                    "./resources/map_tileQT09_v10_staggert_10x10tiles_32x32tilesize_0layers_0tilesets.json")
    @ddtd.test_case("./resources/map_tileQT09_v10_DTD_orthogonal_10x10tiles_32x32tilesize_0layers_0tilesets.tmx",
                    "./resources/map_tileQT09_v10_DTD_orthogonal_10x10tiles_32x32tilesize_0layers_0tilesets.json")
    @ddtd.test_case("./resources/map_tileQT09_v10_DTD_orthogonal_10x10tiles_32x32tilesize_0layers_0tilesets_bgFF00FF.tmx",
                    "./resources/map_tileQT09_v10_DTD_orthogonal_10x10tiles_32x32tilesize_0layers_0tilesets_bgFF00FF.json")
    @ddtd.test_case("./resources/map_tileQT09_v10_isometric_10x10tiles_32x32tilesize_0layers_0tilesets.tmx",
                    "./resources/map_tileQT09_v10_isometric_10x10tiles_32x32tilesize_0layers_0tilesets.json")
    @ddtd.test_case("./resources/map_tileQT09_v10_orthogonal_10x10tiles_32x32tilesize_0layers_0tilesets.tmx",
                    "./resources/map_tileQT09_v10_orthogonal_10x10tiles_32x32tilesize_0layers_0tilesets.json")
    @ddtd.test_case("./resources/map_tileQT09_v10_orthogonal_tilesets_01.tmx",
                    "./resources/map_tileQT09_v10_orthogonal_tilesets_01.json")
    @ddtd.test_case("./resources/map_tileQT09_v10_orthogonal_tilesets_02.tmx",
                    "./resources/map_tileQT09_v10_orthogonal_tilesets_02.json")
    @ddtd.test_case("./resources/map_tileQT09_v10_orthogonal_map_properties.tmx",
                    "./resources/map_tileQT09_v10_orthogonal_map_properties.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_01.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_01.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_02.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_02.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_03_relative_path.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_03_relative_path.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_04_relative_path_parent.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_04_relative_path_parent.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_05_relative_path_parent.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_05_relative_path_parent.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_06_relative_path.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_06_relative_path.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_07_relative_path.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_07_relative_path.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_08.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_08.json")
    # @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_09.tmx",
    #                 "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_09.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_10.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_10.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_11.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_11.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_12.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_12.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_13.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_13.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_14.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_14.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_15.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_15.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_16.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_16.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_17.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_17.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_18.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_18.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_19.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_19.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_20.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_20.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_21.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_21.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_22.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_22.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_23_offset.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_23_offset.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_24.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_24.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_25.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_25.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_26.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_26.json")
    @ddtd.test_case("./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_27.tmx",
                    "./resources/map_tiledQT09_v10_orthogonal_tileset_tsx_27.json")
    def test_map_element(self, tmx_file_to_parse, json_file_name):
        # import os
        # print(os.getcwd())
        # arrange
        json_file = open(json_file_name)
        expected = json.load(json_file)

        # act
        actual = mut.convert_tmx_to_json(tmx_file_to_parse)

        # verify
        self.maxDiff = None
        self.assertDictEqual(expected, actual)

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()
