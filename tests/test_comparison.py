# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pytmxloader
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import unittest
import operator

import comparison as mut  # module under test
from comparison import EquatableMixin
from comparison import ComparableMixin
from comparison import NotComparableException

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class AltA(EquatableMixin):  # this is a mutable type
    def __init__(self, value):
        self.value = value

    def __eq__(self, other):
        if isinstance(other, AltA):
            return self.value == other.value
        return NotImplemented


class AltOtherTypeSameValAsA(EquatableMixin):  # this is a mutable type
    def __init__(self, value):
        self.value = value

    def __eq__(self, other):
        if isinstance(other, AltOtherTypeSameValAsA):
            return self.value == other.value
        return NotImplemented


class AltB(object):  # this is a mutable type
    def __init__(self, value):
        self.val = value

    def __eq__(self, other):
        if isinstance(other, AltB):
            return self.val == other.val
        elif isinstance(other, AltA):  # knows how to compare to A
            return self.val == other.value
        else:
            return NotImplemented

    def __ne__(self, other):
        eq = self.__eq__(other)
        if eq is NotImplemented:
            return NotImplemented
        return not eq


class AltHashableO(EquatableMixin):
    def __init__(self, value):
        self.value = value

    def __eq__(self, other):
        if isinstance(other, AltHashableO):
            return self.value == other.value
        return NotImplemented

    def __hash__(self):  # define this only for immutable types
        return hash(self.value)


class AltMultipleValues(ComparableMixin):

    def __init__(self, val1, val2, val3):
        self._val1 = val1
        self._val2 = val2
        self._val3 = val3

    def __eq__(self, other):
        if isinstance(other, AltMultipleValues):
            return self._val1 == other._val1 and self._val2 == other._val2 and self._val3 == other._val3
            # return (self._val1, self._val2, self._val3) == (other._val1, other._val2, other._val3)
        return NotImplemented

    def __lt__(self, other):
        if isinstance(other, AltMultipleValues):
            return (self._val1, self._val2, self._val3) < (other._val1, other._val2, other._val3)
        return NotImplemented

    def __hash__(self):  # define this only for immutable types
        return hash(self._val1) ^ hash(self._val2) ^ hash(self._val3)


class TestEquality(unittest.TestCase):

    def setUp(self):
        self.instance_a = AltA(1)
        self.other_eq_a = AltA(1)
        self.different_a = AltA(3)

        self.different_type_as_a_but_same_value = AltOtherTypeSameValAsA(1)

        self.value_of_hashable_o = 4
        self.hashable_o = AltHashableO(self.value_of_hashable_o)
        self.other_hashable_o = AltHashableO(self.value_of_hashable_o)
        self.different_o = AltHashableO(7)

        self.multi_val = AltMultipleValues(1, 2, 3)
        self.multi_val_hash = hash(1) ^ hash(2) ^ hash(3)  # we know this implementation detail only for testing
        self.other_eq_multi_val = AltMultipleValues(1, 2, 3)
        self.different_multi_val = AltMultipleValues(5, 6, 7)

        self.type_b_instance = AltB(1)

    def test_A_is_equal(self):
        # act/verify
        self.assertTrue(self.instance_a == self.other_eq_a)

    def test_A_is_not_equal(self):
        # act/verify
        self.assertFalse(self.instance_a == self.different_a)

    def test_A_is_not_different(self):
        # act/verify
        self.assertFalse(self.instance_a != self.other_eq_a)

    def test_A_is_different(self):
        # act/verify
        self.assertTrue(self.instance_a != self.different_a)

    def test_A_is_not_other_A(self):
        # act/verify
        self.assertFalse(self.instance_a is self.other_eq_a)

    def test_A_is_A(self):
        # act/verify
        self.assertTrue(self.instance_a is self.instance_a)

    def test_ordering_A(self):
        # arrange
        a = self.instance_a
        other = self.other_eq_a

        # act/verify
        self.assertRaises(NotComparableException, sorted, [other, a])

    def test_A_lt_B(self):
        # act/verify
        self.assertRaises(NotComparableException, operator.lt, self.instance_a, self.other_eq_a)

    def test_usage_of_A_as_hashed_key(self):
        # arrange
        a = self.instance_a
        b = self.different_a

        # act/verify
        self.assertRaises(TypeError, set, [a, b])

    def test_equal_hashable_object_have_same_hash_too(self):
        # acct/verify
        self.assertEqual(self.hashable_o, self.other_hashable_o)
        self.assertEqual(hash(self.hashable_o), hash(self.other_hashable_o))

    def test_hash_of_A(self):
        # arrange
        a = self.instance_a

        # act/verify
        self.assertRaises(TypeError, hash, a)

    def test_a_vs_other_type_same_value(self):
        """
        Add an extra isinstance check for this type. In my opinion its clearer and more readable and its less
        error prone to compare two completely different types that may have the same value by chance.

        e.g. change AltA to this:

            def __eq__(self, other):
                if isinstance(other, AltA):
                    return self.value == other.value
                elif isinstance(other, AltOtherTypeSameValAsA):  # <-- explicit extra check for different type
                    return self.value == other.value
                return NotImplemented

        """
        # act/verify
        self.assertFalse(self.instance_a == self.different_type_as_a_but_same_value)  # note the assertFalse!!

    def test_hash_of_B(self):
        # arrange
        b = self.hashable_o
        expected = hash(self.value_of_hashable_o)  # this only works because we know the internals and what we hash

        # act
        actual = hash(b)

        # verify
        self.assertEqual(expected, actual)

    def test_usage_of_HashableO_as_hashed_key(self):
        # arrange
        a = self.hashable_o
        b = self.different_o
        expected = {b, a}  # set()

        # act
        actual = {a, b}  # set()

        # verify
        self.assertEqual(expected, actual)

    def test_multiple_values_equality(self):
        # arrange
        a = self.multi_val
        b = self.other_eq_multi_val

        # act/verify
        self.assertEqual(a, b)

    def test_multiple_values_are_different(self):
        # arrange
        a = self.multi_val
        b = self.different_multi_val

        # act/verify
        self.assertNotEqual(a, b)

    def test_multiple_values_are_hashable(self):
        # arrange
        a = self.multi_val
        expected = self.multi_val_hash

        # act
        actual = hash(a)

        # verify
        self.assertEqual(expected, actual)

    def test_A_equal_B(self):
        # arrange
        a = self.instance_a
        b = self.type_b_instance
        expected = True

        # act
        actual = a == b  # Type 'B' doesn't have expected attribute 'get_compare_key'  <- pycharm warning

        # verify
        self.assertEqual(expected, actual)

    def test_B_equal_A(self):
        # arrange
        a = self.instance_a
        b = self.type_b_instance
        expected = True

        # act
        actual = b == a

        # verify
        self.assertEqual(expected, actual)

    def test_comparable_multi_val__lt__other_multi_val(self):
        # act/verify
        self.assertTrue(self.multi_val < self.different_multi_val)

    def test_comparable_multi_val__le__other_multi_val(self):
        # act/verify
        self.assertTrue(self.multi_val <= self.different_multi_val)

    def test_comparable_multi_val__le___multi_val(self):
        # act/verify
        self.assertTrue(self.multi_val <= self.multi_val)

    def test_comparable_multi_val__gt__other_multi_val(self):
        # act/verify
        self.assertTrue(self.different_multi_val > self.multi_val)

    def test_comparable_multi_val__ge___multi_val(self):
        # act/verify
        self.assertTrue(self.multi_val >= self.multi_val)




if __name__ == '__main__':
    unittest.main()
