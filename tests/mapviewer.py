# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function


try:
    import sys
    import traceback
    import json
    import math
    import time

    import pygame
    import pytmxloader

    __version__ = '1.0.0.0'

    # for easy comparison as in sys.version_info but digits only
    __version_info__ = tuple([int(d) for d in __version__.split('.')])

    __author__ = "DR0ID"
    __email__ = "dr0iddr0id {at} gmail [dot] com"
    __copyright__ = "DR0ID @ 2016"
    __credits__ = ["DR0ID"]  # list of contributors
    __maintainer__ = "DR0ID"
    __license__ = "New BSD license"

    __all__ = []  # list of public visible parts of this module 

    SCREENSIZE = 1024, 1024

    import os
    import sys

    class Sprite(object):

        def __init__(self, x, y, image, layer, orig_y):
            self.image = image
            self.pos = (x, y)
            self.layer = layer
            self.orig_y = orig_y


    def create_sprite_layers(map_info):
        dummy_image = pygame.Surface((32, 32))
        dummy_image.fill((255, 255, 0))
        sprite_layers = []  # sprite_layers[0][y][x]
        for idx_layer, layer in enumerate(map_info.layers):
            print("loading layer " + layer.name)
            # noinspection SpellCheckingInspection
            if layer.layer_type == "tilelayer":
                layer_ = []
                for idx_y, row in enumerate(layer.data_yx):
                    layer_.append([])
                    for lti in row:
                        if lti is not None:
                            image = lti.tile_info.image
                            # r = image.get_rect()
                            # if r.w > 32 or r.h > 32:
                            #     pygame.draw.rect(image, (255, 0, 0), r, 2)
                            sprite = Sprite(lti.tile_x, lti.tile_y + lti.offset_y, image, idx_layer, lti.tile_y)
                            layer_[idx_y].append(sprite)
                        else:
                            # TODO: how to avoid dummy sprites?
                            sprite = Sprite(-32, -32, dummy_image, idx_layer, -32)
                            layer_[idx_y].append(sprite)
                sprite_layers.append((1, layer_))
            elif isinstance(layer, pytmxloader.ObjectLayerInfo):
                sprite_layers.append((2, layer))
            elif isinstance(layer, pytmxloader.ImageLayerInfo):
                sprite_layers.append((3, layer))

        return sprite_layers


    def load_images(map_info, img_cache, map_path_to_load):
        if img_cache is None:
            img_cache = {}
        map_path_dir = os.path.dirname(map_path_to_load)

        for layer in map_info.layers:
            # noinspection SpellCheckingInspection
            if layer.layer_type == "imagelayer":
                img = img_cache.get(layer.image_path_rel_to_cwd, None)
                if img is None:
                    # noinspection PyUnresolvedReferences
                    img = pygame.image.load(layer.image_path_rel_to_cwd).convert_alpha()
                    img.fill((255, 255, 255, int(round(255.0 * layer.opacity))), None, pygame.BLEND_RGBA_MULT)
                    img_cache[layer.image_path_rel_to_cwd] = img
                layer.image = img

        for tile in map_info.tiles.values():
            tileset = tile.tileset
            if tileset.image_path_rel_to_cwd:
                image_name = tileset.image_path_rel_to_cwd
            else:
                image_name = tileset.image_path_rel_to_map
                image_name = os.path.abspath(os.path.join(map_path_dir, image_name))

            # load source image first if not already cached
            image_source = img_cache.get(image_name, None)
            if image_source is None:
                print("loading image " + image_name)
                # noinspection PyUnresolvedReferences
                image_source = pygame.image.load(image_name)
                if tileset.transparent_color:
                    transparent_color = pygame.Color(tileset.transparent_color)
                    image_source.set_colorkey(transparent_color, pygame.RLEACCEL)
                    image_source = image_source.convert()
                else:
                    image_source = image_source.convert_alpha()
                img_cache[image_name] = image_source

            # load tile image from source image
            key = (image_name, tile.spritesheet_x, tile.spritesheet_y, tile.angle, tile.flip_x, tile.flip_y)
            tile_image = img_cache.get(key, None)
            if tile_image is None:
                tile_image = pygame.Surface((tileset.tile_width, tileset.tile_height), pygame.SRCALPHA)
                # tile_image.fill((0, 0, 0, 0), None, pygame.BLEND_RGBA_MULT)
                source_rect = pygame.Rect(tile.spritesheet_x, tile.spritesheet_y, tileset.tile_width,
                                          tileset.tile_height)
                tile_image.blit(image_source, (0, 0), source_rect)
                # important: order of operations matter: rotate first and then flip the image
                if tile.angle > 0:
                    tile_image = pygame.transform.rotate(tile_image, -tile.angle)
                if tile.flip_x or tile.flip_y:
                    tile_image = pygame.transform.flip(tile_image, tile.flip_x, tile.flip_y)
                img_cache[key] = tile_image
            tile.image = tile_image


    def draw_grid(screen, cam_x, cam_y, map_info):
        layer = map_info.layers[0]
        screen_w, screen_h = screen.get_size()

        color = [100] * 3
        for x in range(cam_x - screen_w, screen_w, layer.tile_w):
            pygame.draw.line(screen, color, (x, 0), (x, screen_h))
        for y in range(cam_y - screen_h, screen_h, layer.tile_h):
            pygame.draw.line(screen, color, (0, y), (screen_w, y))

        color = [150] * 3
        for x in range(cam_x, layer.width * + layer.tile_w + layer.tile_w + cam_x, layer.tile_w):
            pygame.draw.line(screen, color, (x, 0), (x, screen_h))
        for y in range(cam_y, layer.height * layer.tile_h + layer.tile_h + cam_y, layer.tile_h):
            pygame.draw.line(screen, color, (0, y), (screen_w, y))


    def draw_image_layer(layer, dx, dy, screen):
        screen.blit(layer.image, (layer.pixel_offset_x + dx, layer.pixel_offset_y + dy))


    def main():
        t0 = time.time()
        if len(sys.argv) > 1:
            map_path_to_load = sys.argv[1]
        else:
            # map_path_to_load = "./resources/basicMap_offsets.json"
            # map_path_to_load = "./resources/basicMap_with_image_collection.json"
            # map_path_to_load = "./resources/basicMap_tileset_other_directory.json"
            map_path_to_load = "./resources/basicMap_object_layer.json"
            # map_path_to_load = "./resources/basicMap_image_layer.json"
            # map_path_to_load = "./resources/map/001-1.json"

        # MapInfo -> layers:[LayerInfo, ...], tilesets: {gid: TileInfo}
        map_info = pytmxloader.load_map_from_file_path(map_path_to_load)
        t1 = time.time()
        print("map loading took", t1 - t0, "ms")

        pygame.init()
        # screen = pygame.display.set_mode(SCREENSIZE, pygame.RESIZABLE | pygame.SRCALPHA, 32)
        screen = pygame.display.set_mode(SCREENSIZE, pygame.SRCALPHA, 32)

        # load images and create sprites
        _img_cache = {}  # {key: img}
        load_images(map_info, _img_cache, map_path_to_load)

        sprite_layers = create_sprite_layers(map_info)
        layer_visibility = [_layer.visible for _layer in map_info.layers]
        # sprites = [_spr for _layer_idx, _layer in enumerate(sprite_layers) for _spr in _layer if
        #            layer_visibility[_layer_idx]]
        current_layer_idx = 0

        dx = 0
        dy = 0
        tile_w = map_info.layers[0].tile_w
        tile_h = map_info.layers[0].tile_h
        col_tile_count = SCREENSIZE[0] // tile_w + 3  # TODO: instead of 6 it should be max width of sprites
        row_tile_count = SCREENSIZE[1] // tile_h + 6  # TODO: instead of 6 it should be max height of sprites

        # def sort_key(_s): return _s.layer, _s.pos[0], _s.orig_y
        # sprites.sort(key=sort_key)
        grid_toggle = False
        # focus_layer_toggle = False
        pixel_rect_toggle = False

        clock = pygame.time.Clock()
        # noinspection PyArgumentList
        pygame.key.set_repeat(200, 50)
        time_accumulator = 0.0
        running = True
        while running:
            if pygame.key.get_mods() & pygame.KMOD_SHIFT:
                step = 1
            else:
                step = 64

            # events
            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        running = False
                    elif event.key == pygame.K_SPACE:
                        current_layer_idx += 1
                        current_layer_idx %= len(layer_visibility)
                    elif event.key == pygame.K_v:
                        layer_visibility[current_layer_idx] = not layer_visibility[current_layer_idx]
                    elif event.key == pygame.K_DOWN:
                        dy -= step
                    elif event.key == pygame.K_UP:
                        dy += step
                    elif event.key == pygame.K_LEFT:
                        dx += step
                    elif event.key == pygame.K_RIGHT:
                        dx -= step
                    elif event.key == pygame.K_g:
                        grid_toggle = not grid_toggle
                    # elif event.key == pygame.K_f:
                    #     focus_layer_toggle = not focus_layer_toggle
                    elif event.key == pygame.K_b:
                        pixel_rect_toggle = not pixel_rect_toggle

            delta_t = clock.tick() / 1000.0  # seconds
            # current_time = pygame.time.get_ticks() / 1000.0  # seconds

            # draw only the visible part
            col_start = -dx // tile_w - 1
            row_start = -dy // tile_h - 1
            row_end = row_start + row_tile_count
            col_end = col_start + col_tile_count
            for idx, layer_tuple in enumerate(sprite_layers):
                layer_type, layer = layer_tuple
                if layer_visibility[idx]:
                    if layer_type == 1:  # hack: tile layer
                        draw_sprite_layer(layer, dx, dy, col_start, row_start, col_end, row_end, screen)
                    elif layer_type == 2:  # hack: object layer
                        color = (233, 239, 119, 255)
                        thickness = 3
                        draw_object_layer(layer, dx, dy, color, screen, thickness, pixel_rect_toggle)
                    elif layer_type == 3:
                        draw_image_layer(layer, dx, dy, screen)
                    else:
                        print("no render method for layer type " + str(layer_type))

            time_accumulator += delta_t
            if time_accumulator > 1.0:
                time_accumulator -= 1.0
                print(clock.get_fps())
                # print(col_start, row_start)

            if grid_toggle:
                draw_grid(screen, dx, dy, map_info)
            pygame.display.flip()
            screen.fill((255, 0, 255))

        pygame.quit()


    def draw_sprite_layer(layer, dx, dy, col_start, row_start, col_end, row_end, screen):
        screen_blit = screen.blit
        for y_idx in range(row_start, row_end):
            for x_idx in range(col_start, col_end):
                try:
                    spr = layer[y_idx][x_idx]
                    screen_blit(spr.image, (spr.pos[0] + dx, spr.pos[1] + dy))
                except IndexError:
                    pass


    def draw_object_layer(layer, dx, dy, color, screen, thickness, pixel_rect_toggle):
        for r in layer.rectangles:
            draw_rect_object(r, dx, dy, color, screen, thickness)
        for e in layer.ellipses:
            draw_ellipse_object(e, dx, dy, color, screen, thickness)
        for p in layer.polygons:
            draw_polygon_object(p, dx, dy, color, screen, thickness)
        for p in layer.poly_lines:
            draw_polyline_object(p, dx, dy, color, screen, thickness)
        for t in layer.tiles:
            draw_tile_object(t, dx, dy, screen)
        if pixel_rect_toggle:
            for o in layer.objects:
                _rect = pygame.Rect(o.pixel_rect)
                pygame.draw.rect(screen, (0, 255, 0), _rect.move(dx, dy), thickness)


    def draw_rect_object(rect_object, dx, dy, color, screen, thickness):
        _shifted_points = [(_p[0] + dx, _p[1] + dy) for _p in rect_object.pixel_points]
        pygame.draw.polygon(screen, color, _shifted_points, thickness)


    def draw_ellipse_object(ellipse_object, dx, dy, color, screen, thickness):
        rect = pygame.Rect(0, 0, ellipse_object.width, ellipse_object.height)
        surf = pygame.Surface(rect.size, flags=pygame.SRCALPHA, depth=32)
        # noinspection PyArgumentEqualDefault
        surf.fill((0, 0, 0, 0), None, pygame.BLEND_RGBA_MULT)
        _thickness = thickness if 2 * thickness < rect.height else rect.height // 2
        _thickness = _thickness if 2 * _thickness < rect.width else rect.width // 2
        pygame.draw.ellipse(surf, color, rect, _thickness)
        rx, ry = pytmxloader.rotate_point(ellipse_object.width / 2.0, ellipse_object.height / 2.0,
                                          ellipse_object.rotation)
        rotated = pygame.transform.rotozoom(surf, -ellipse_object.rotation, 1.0)
        rotated_rect = rotated.get_rect(center=(ellipse_object.x + rx + dx, ellipse_object.y + ry + dy))
        screen.blit(rotated, rotated_rect)


    def draw_polygon_object(polygon_object, dx, dy, color, screen, thickness):
        _shifted_points = [(_p[0] + dx, _p[1] + dy) for _p in polygon_object.pixel_points]
        pygame.draw.polygon(screen, color, _shifted_points, thickness)


    def draw_polyline_object(polyline_object, dx, dy, color, screen, thickness):
        _shifted_points = [(_p[0] + dx, _p[1] + dy) for _p in polyline_object.pixel_points]
        pygame.draw.lines(screen, color, False, _shifted_points, thickness)


    def draw_tile_object(tile_object, dx, dy, screen):
        # TODO: depending on draw type this has to be done differently
        # from the docs:
        # The image alignment currently depends on the map orientation. In orthogonal orientation it's aligned to
        # the bottom-left while in isometric it's aligned to the bottom-center.
        #
        scaled = pygame.transform.scale(tile_object.tile_info.image, (int(tile_object.width), int(tile_object.height)))
        surf = pygame.Surface(scaled.get_size(), pygame.SRCALPHA, 32)
        # noinspection PyArgumentEqualDefault
        surf.fill((0, 0, 0, 0), None, pygame.BLEND_RGBA_MULT)
        surf.blit(scaled, (0, 0))
        rx, ry = pytmxloader.rotate_point(tile_object.width / 2.0, -tile_object.height / 2.0, tile_object.rotation)
        rotated = pygame.transform.rotozoom(scaled, -tile_object.rotation, 1)
        rotated_rect = rotated.get_rect(center=(tile_object.x + rx + dx, tile_object.y + ry + dy))
        screen.blit(rotated, rotated_rect)


    if __name__ == '__main__':
        main()

except Exception as ex:
    # trying to catch any exceptions and print them out in a way that the user will see them!
    sys.stdout.flush()
    sys.stderr.write("Error:")
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    sys.stderr.write(str(ex.__class__.__name__))
    sys.stderr.write(str(ex))
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback, file=sys.stderr)
    sys.stderr.write('\n')
    sys.stderr.write('Press any key... \n')
    sys.stderr.flush()
