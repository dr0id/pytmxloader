# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pytmxloader
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import os
import unittest

try:
    # noinspection PyUnresolvedReferences
    from io import StringIO, BytesIO
except ImportError:
    # noinspection PyUnresolvedReferences
    from StringIO import StringIO

import pytmxloader.pytmxloader as mut  # module under test

__version__ = '4.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")

# noinspection SpellCheckingInspection
map__basicMap_tileset_other_directory__as_string = u'{"layers": [{"opacity": 1, "name": "Tile Layer 1", "width": 3, ' \
                                                   '"type": "tilelayer", "offsetx": 0, "offsety": 0, "height": 3, ' \
                                                   '"visible": true, "y": 0, "x": 0, "data": ' \
                                                   '[2, 2147483650, 1073741826, ' \
                                                   '3221225474, 0, 2684354562, ' \
                                                   '536870914, 3758096386, 1610612738]}], ' \
                                                   '"orientation": "orthogonal", "tileheight": 32, "tilewidth": 32, ' \
                                                   '"nextobjectid": 1, "width": 3, "renderorder": "right-down", ' \
                                                   '"version": 1, "tilesets": [{"name": "basicTileset", ' \
                                                   '"tileheight": 32, "tilewidth": 32, ' \
                                                   '"image": "../basicTileset.png", "spacing": 0, ' \
                                                   '"tilecount": 4, "imageheight": 42, "imagewidth": 138, ' \
                                                   '"firstgid": 1, "margin": 5, "properties": {}}], ' \
                                                   '"height": 3, "properties": {}}'


def _create_basic_map_info():
    img_rel_path_to_cur = os.path.join("resources", "basicTileset.png")
    tileset = mut.TilesetInfo("basicTileset", 32, 32, {}, "basicTileset.png", img_rel_path_to_cur, 1, None, 0, 0)
    # noinspection PyArgumentEqualDefault
    # tileset, spritesheet_x, spritesheet_y, properties, angle=0, flip_x=False, flip_y=False
    _tiles = {
        1: mut.TileInfo(tileset, 5 + 0 * 32, 5 + 0 * 32, {}),
        2: mut.TileInfo(tileset, 5 + 1 * 32, 5 + 0 * 32, {}),
        3: mut.TileInfo(tileset, 5 + 2 * 32, 5 + 0 * 32, {}),
        4: mut.TileInfo(tileset, 5 + 3 * 32, 5 + 0 * 32, {}),
        536870914: mut.TileInfo(tileset, 5 + 1 * 32, 5 + 0 * 32, {}, 90, True, False),
        1073741826: mut.TileInfo(tileset, 5 + 1 * 32, 5 + 0 * 32, {}, 0, False, True),
        1610612738: mut.TileInfo(tileset, 5 + 1 * 32, 5 + 0 * 32, {}, 270, False, False),
        2147483650: mut.TileInfo(tileset, 5 + 1 * 32, 5 + 0 * 32, {}, 0, True, False),
        2684354562: mut.TileInfo(tileset, 5 + 1 * 32, 5 + 0 * 32, {}, 90, False, False),
        3221225474: mut.TileInfo(tileset, 5 + 1 * 32, 5 + 0 * 32, {}, 180, False, False),
        3758096386: mut.TileInfo(tileset, 5 + 1 * 32, 5 + 0 * 32, {}, 270, True, False),
    }
    _data = [
        [
            mut.TileLayerTileInfo(0 * 32, 0 * 32, _tiles[2], 0, 0),
            mut.TileLayerTileInfo(1 * 32, 0 * 32, _tiles[2147483650], 0, 0),
            mut.TileLayerTileInfo(2 * 32, 0 * 32, _tiles[1073741826], 0, 0),
        ],
        [
            mut.TileLayerTileInfo(0 * 32, 1 * 32, _tiles[3221225474], 0, 0),
            None,
            mut.TileLayerTileInfo(2 * 32, 1 * 32, _tiles[2684354562], 0, 0),
        ],
        [
            mut.TileLayerTileInfo(0 * 32, 2 * 32, _tiles[536870914], 0, 0),
            mut.TileLayerTileInfo(1 * 32, 2 * 32, _tiles[3758096386], 0, 0),
            mut.TileLayerTileInfo(2 * 32, 2 * 32, _tiles[1610612738], 0, 0),
        ],
    ]
    # name, x, y, width, height, tile_w, tile_h, visible, opacity, layer_type, data):
    _layers = [mut.TileLayerInfo("Tile Layer 1", 0, 0, 3, 3, 32, 32, True, 1, _data, {}, 0, 0)]
    return mut.MapInfo(_layers, _tiles, {})


# class TmxLoaderAssertions(unittest.TestCase):

# noinspection PyTypeChecker
def assert_animation_info_equal(self, expected, actual, key):
    if expected is None and actual is not None:
        raise AssertionError("expected should have animation frames " + str(key))
    if expected is not None and actual is None:
        raise AssertionError("actual should have animation frames" + str(key))

    if expected is None and actual is None:
        return

    if len(expected) != len(actual):
        raise AssertionError(
            "Not same number of animation frames: {0} != {1} for TileInfo {2}".format(len(expected), len(actual), key))

    for idx, exp_item in enumerate(expected):
        act_item = actual[idx]
        # assert_tile_info_equal(self, exp_item.tile_info, act_item.tile_info, key)
        self.assertEqual(exp_item.duration, act_item.duration)
        self.assertEqual(exp_item, act_item)


def assert_tileset_equal(self, expected, actual):
    self.assertEqual(expected.pixel_offset_x, actual.pixel_offset_x)
    self.assertEqual(expected.pixel_offset_y, actual.pixel_offset_y)
    self.assertEqual(expected.name, actual.name)
    self.assertEqual(expected.tile_width, actual.tile_width)
    self.assertEqual(expected.tile_height, actual.tile_height)
    self.assertEqual(expected.properties, actual.properties)
    self.assertEqual(expected.image_path_rel_to_map, actual.image_path_rel_to_map)
    self.assertEqual(expected.first_gid, actual.first_gid)
    self.assertEqual(expected.transparent_color, actual.transparent_color)
    self.assertEqual(expected.image_path_rel_to_cwd, actual.image_path_rel_to_cwd)


def assert_tile_info_equal(self, expected, actual, key):
    assert_tileset_equal(self, expected.tileset, actual.tileset)
    self.assertEqual(expected.tileset, actual.tileset)
    self.assertEqual(expected.flip_x, actual.flip_x)
    self.assertEqual(expected.flip_y, actual.flip_y)
    self.assertEqual(expected.angle, actual.angle)
    assertion_msg_properties = "{0} != {1} for TileInfo {2}".format(expected.properties, actual.properties, key)
    self.assertDictEqual(expected.properties, actual.properties, assertion_msg_properties)
    assertion_msg_spritesheet_x = "spritesheet_x: {0} != {1} for TileInfo {2}".format(expected.spritesheet_x,
                                                                                      actual.spritesheet_x, key)
    self.assertEqual(expected.spritesheet_x, actual.spritesheet_x, assertion_msg_spritesheet_x)
    assertion_msg_spritesheet_y = "spritesheet_y: {0} != {1} for TileInfo {2}".format(expected.spritesheet_y,
                                                                                      actual.spritesheet_y, key)
    self.assertEqual(expected.spritesheet_y, actual.spritesheet_y, assertion_msg_spritesheet_y)
    assertion_msg_probability = "{0} != {1} for TileInfo {2}".format(expected.probability, actual.probability, key)
    self.assertEqual(expected.probability, actual.probability, assertion_msg_probability)
    assert_animation_info_equal(self, expected.animation, actual.animation, key)
    self.assertEqual(expected, actual)


def assert_tile_layer_info_equal(self, expected, actual, idx):
    assert_common_layer_attributes_are_equal(self, expected, actual, idx)
    self.assertEqual(expected.tile_w, actual.tile_w)
    self.assertEqual(expected.tile_h, actual.tile_h)
    if len(expected.data_yx) != len(actual.data_yx):
        raise AssertionError("data of TileLayerInfo differs")
    for idx_y, exp_row in enumerate(expected.data_yx):
        act_row = actual.data_yx[idx_y]
        if len(exp_row) != len(act_row):
            raise AssertionError("row {0} length differs".format(idx_y))
        for idx_x, exp_tile_layer_tile_info in enumerate(exp_row):
            act_tile_layer_tile_info = act_row[idx_x]
            if exp_tile_layer_tile_info is not None and act_tile_layer_tile_info is not None:
                self.assertEqual(exp_tile_layer_tile_info.tile_x, act_tile_layer_tile_info.tile_x)
                self.assertEqual(exp_tile_layer_tile_info.tile_y, act_tile_layer_tile_info.tile_y)
                self.assertEqual(exp_tile_layer_tile_info.offset_x, act_tile_layer_tile_info.offset_x)
                self.assertEqual(exp_tile_layer_tile_info.offset_y, act_tile_layer_tile_info.offset_y)
                self.assertEqual(exp_tile_layer_tile_info.tile_info, act_tile_layer_tile_info.tile_info)
            self.assertEqual(exp_tile_layer_tile_info, act_tile_layer_tile_info)
    self.assertEqual(expected, actual)
    self.assertSequenceEqual(expected.data_yx, actual.data_yx)


def assert_image_layer_are_equal(self, expected, actual, idx):
    assert_common_layer_attributes_are_equal(self, expected, actual, idx)

    self.assertEqual(expected.image_path_rel_to_map, actual.image_path_rel_to_map)
    self.assertEqual(expected.image_path_rel_to_cwd, actual.image_path_rel_to_cwd)

    self.assertEqual(expected, actual)


def assert_common_layer_attributes_are_equal(self, expected, actual, idx):
    self.assertEqual(expected.layer_type, actual.layer_type)
    self.assertDictEqual(expected.properties, actual.properties)
    self.assertEqual(expected.name, actual.name)
    self.assertEqual(expected.pixel_offset_x, actual.pixel_offset_x,
                     "{0} != {1} for layer info[{3}]: '{2}'".format(expected.pixel_offset_x,
                                                                    actual.pixel_offset_x, expected.name, idx))
    self.assertEqual(expected.pixel_offset_y, actual.pixel_offset_y)
    self.assertEqual(expected.width, actual.width)
    self.assertEqual(expected.height, actual.height)
    self.assertEqual(expected.visible, actual.visible)
    self.assertEqual(expected.opacity, actual.opacity)
    self.assertEqual(expected.tile_offset_x, actual.tile_offset_x)
    self.assertEqual(expected.tile_offset_y, actual.tile_offset_y)


def assert_object_layer_are_equal(self, expected, actual, idx):
    assert_common_layer_attributes_are_equal(self, expected, actual, idx)

    self.assertEqual(expected.draw_order, actual.draw_order)

    self.assertEqual(len(expected.objects), len(actual.objects))
    self.assertEqual(expected.objects, actual.objects)

    self.assertEqual(expected, actual)


def assert_map_info_equal(self, expected, actual):
    self.maxDiff = None
    self.assertDictEqual(expected.properties, actual.properties, "map info properties differ")
    exp_tiles = expected.tiles
    act_tiles = actual.tiles
    if len(exp_tiles) != len(act_tiles):
        raise AssertionError("tiles count differ in length:  {0} != {1}".format(len(exp_tiles), len(act_tiles)))
    for exp_key, exp_val in exp_tiles.items():
        act_val = act_tiles[exp_key]
        assert_tile_info_equal(self, exp_val, act_val, exp_key)

    # self.assertSequenceEqual(expected.layers, actual.layers,)
    exp_layers = expected.layers
    act_layers = actual.layers
    if len(exp_layers) != len(act_layers):
        raise AssertionError("layers differ in length:  {0} != {1}".format(len(exp_layers), len(act_layers)))
    for idx, exp_item in enumerate(exp_layers):
        act_item = act_layers[idx]
        if exp_item.layer_type == mut.TILE_LAYER_TYPE:
            assert_tile_layer_info_equal(self, exp_item, act_item, idx)
        elif exp_item.layer_type == mut.IMAGE_LAYER_TYPE:
            assert_image_layer_are_equal(self, exp_item, act_item, idx)
        elif exp_item.layer_type == mut.OBJECT_LAYER_TYPE:
            assert_object_layer_are_equal(self, exp_item, act_item, idx)
        else:
            self.fail("unknown layer type: " + exp_item.layer_type)

    self.assertEqual(expected, actual)


class TestTmxLoaderAssertMethods(unittest.TestCase):
    # noinspection SpellCheckingInspection
    def test_assert_map_info_equal(self):
        # arrange
        tlti1 = mut.TileLayerTileInfo(1, 2, None, 3, 4)
        tlti2 = mut.TileLayerTileInfo(1, 2, None, 3, 4)
        tlti3 = mut.TileLayerTileInfo(1, 2, None, 3, 4)

        tli1 = mut.TileLayerInfo("Tile Layer 1", 0, 0, 3, 3, 32, 32, True, 1, [[tlti1, tlti2, tlti3]], {}, 0, 0)
        tli2 = mut.TileLayerInfo("Tile Layer 1", 0, 0, 3, 3, 32, 32, True, 1, [[tlti1, tlti2, tlti3]], {}, 0, 0)
        tli3 = mut.TileLayerInfo("Tile Layer 1", 0, 0, 3, 3, 32, 32, True, 1, [[tlti1, tlti2, tlti3]], {}, 0, 0)
        exp = mut.MapInfo([tli1, tli2, tli3], {}, {})
        act = mut.MapInfo([tli1, tli2, tli3], {}, {})

        # act/verify
        try:
            # self.sut.assert_map_info_equal(self, exp, act)
            assert_map_info_equal(self, exp, act)
        except AssertionError as ae:
            self.fail("MapInfo should be same " + str(ae))

    def test_assert_map_info_raises_different_layers(self):
        # arrange
        tli1 = mut.TileLayerInfo("Tile Layer 1", 0, 0, 3, 3, 32, 32, False, 1, [1, 2, 3], {}, 0, 0)
        tli2 = mut.TileLayerInfo("Tile Layer 1", 0, 0, 3, 3, 32, 32, True, 1, [1, 2, 3], {}, 0, 0)
        expected = mut.MapInfo([tli1], {}, {})
        actual = mut.MapInfo([tli2], {}, {})

        # act
        #  verify
        self.assertRaises(AssertionError, assert_map_info_equal, self, expected, actual)

    def test_assert_map_info_raises_different_layers_different_order(self):
        # arrange
        tli1 = mut.TileLayerInfo("Tile Layer 1", 0, 0, 3, 3, 32, 32, True, 1, [1, 2, 3], {}, 0, 0)
        tli2 = mut.TileLayerInfo("Tile Layer 2", 0, 0, 3, 3, 32, 32, True, 1, [1, 2, 3], {}, 0, 0)
        tli3 = mut.TileLayerInfo("Tile Layer 3", 0, 0, 3, 3, 32, 32, True, 1, [1, 2, 3], {}, 0, 0)

        expected = mut.MapInfo([tli1, tli2, tli3], {}, {})
        actual = mut.MapInfo([tli3, tli2, tli1], {}, {})

        # act
        #  verify
        self.assertRaises(AssertionError, assert_map_info_equal, self, expected, actual)

    def test_assert_map_info_raises_different_tiles(self):
        # arrange
        tileset = mut.TilesetInfo("tileset", 32, 64, {}, "path rel to map", "", 1001, "#00ff00", 33, 44)
        expected = mut.MapInfo([1], {1: mut.TileInfo(tileset, 2, 3, {})}, {})
        actual = mut.MapInfo([1], {1: mut.TileInfo(tileset, 9, 9, {})}, {})

        # act
        #  verify
        self.assertRaises(AssertionError, assert_map_info_equal, self, expected, actual)


def _get_pixel_rect_from_points(pixel_points):
    _min_x = min([_px for _px, _py in pixel_points])
    _min_y = min([_py for _px, _py in pixel_points])
    return (_min_x, _min_y, max([_px for _px, _py in pixel_points]) - _min_x,
            max([_py for _px, _py in pixel_points]) - _min_y)


class BasicMapLoadingShould(unittest.TestCase):
    def setUp(self):
        # self.longMessage = True
        # self.maxDiff = None
        self.expected_basic_map_info = _create_basic_map_info()

    def test_load_tiled_layer_csv(self):
        # act
        actual = mut.load_map_from_file_path("./resources/basicMap_csv.json")

        # verify
        assert_map_info_equal(self, self.expected_basic_map_info, actual)

    def test_load_tiled_layer_base64(self):
        # act
        actual = mut.load_map_from_file_path("./resources/basicMap_base64.json")

        # verify
        assert_map_info_equal(self, self.expected_basic_map_info, actual)

    def test_load_tiled_layer_gzip(self):
        # act
        actual = mut.load_map_from_file_path("./resources/basicMap_gzip.json")

        # verify
        assert_map_info_equal(self, self.expected_basic_map_info, actual)

    def test_load_tiled_layer_zlib(self):
        # act
        actual = mut.load_map_from_file_path("./resources/basicMap_zlib.json")

        # verify
        assert_map_info_equal(self, self.expected_basic_map_info, actual)

    def test_load_properties(self):
        # arrange
        expected = self.expected_basic_map_info
        expected.properties = {"p1": "p11"}
        expected.layers[0].properties = {"p2": "p22"}
        expected.tiles[1].tileset.properties = {"p4": "p44"}
        expected.tiles[1].properties = {"p3": "p33"}

        # act
        actual = mut.load_map_from_file_path("./resources/basicMap_with_properties.json")

        # verify
        # self.assertEqual(expected, actual)
        assert_map_info_equal(self, expected, actual)
        self.assertEqual("p11", actual.properties["p1"])
        self.assertEqual("p22", actual.layers[0].properties["p2"])
        self.assertEqual("p33", actual.tiles[1].properties["p3"])
        self.assertEqual("p44", actual.tiles[1].tileset.properties["p4"])

    def test_load_tiled_layer_offsets(self):
        # arrange
        expected = self.expected_basic_map_info
        _layer_info = expected.layers[0]
        _layer_info.pixel_offset_x = 2
        _layer_info.pixel_offset_y = 3
        _layer_info.tile_offset_x = 4
        _layer_info.tile_offset_y = 5
        _tileset = expected.tiles[1].tileset
        _tileset.pixel_offset_x = 3
        _tileset.pixel_offset_y = 4
        data = _layer_info.data_yx
        for row in data:
            for col in row:
                if col is not None:
                    col.tile_x += _tileset.pixel_offset_x + _layer_info.pixel_offset_x + _layer_info.tile_offset_x * 32
                    col.tile_y += _tileset.pixel_offset_y + _layer_info.pixel_offset_y + _layer_info.tile_offset_y * 32
        expected.tiles[1].probability = 0.5
        expected.tiles[3].probability = 0.5

        # act
        actual = mut.load_map_from_file_path("./resources/basicMap_offsets.json")

        # verify
        assert_map_info_equal(self, expected, actual)
        self.assertEqual(_layer_info.pixel_offset_x, actual.layers[0].pixel_offset_x)
        self.assertEqual(_layer_info.pixel_offset_y, actual.layers[0].pixel_offset_y)
        self.assertEqual(self.expected_basic_map_info, actual)
        self.assertEqual(_tileset.pixel_offset_x, actual.tiles[1].tileset.pixel_offset_x)
        self.assertEqual(_tileset.pixel_offset_y, actual.tiles[1].tileset.pixel_offset_y)

    def test_load_tiled_layer_with_animation(self):
        # arrange
        expected = self.expected_basic_map_info
        tileset = expected.tiles[1].tileset
        expected.tiles[2147483651] = mut.TileInfo(tileset, 5 + 2 * 32, 5 + 0 * 32, {"p9": "p99"}, flip_x=True)
        expected.layers[0].data_yx[1][1] = mut.TileLayerTileInfo(1 * 32, 1 * 32, expected.tiles[2147483651], 0, 0)
        expected.tiles[3].properties = {"p9": "p99"}
        expected.tiles[3].probability = 0.75
        expected.tiles[2147483651].probability = 0.75
        expected.tiles[3].animation = [mut.AnimationFrameInfo(expected.tiles[1], 500),
                                       mut.AnimationFrameInfo(expected.tiles[3], 250)]
        expected.tiles[2147483651].animation = [mut.AnimationFrameInfo(expected.tiles[1], 500),
                                                mut.AnimationFrameInfo(expected.tiles[3], 250)]

        # act
        actual = mut.load_map_from_file_path("./resources/basicMap_with_animation.json")

        # verify
        assert_map_info_equal(self, expected, actual)

    def test_load_map_with_tileset_image_in_other_directory(self):
        import os
        from os import path
        # arrange
        expected = self.expected_basic_map_info
        tileset = expected.tiles[1].tileset
        image_rel_to_map = "../basicTileset.png"
        map_path = "./resources/basicMap_tileset_other_directory.json"
        image_rel = path.join(path.dirname(map_path), image_rel_to_map)
        image_rel_to_cur = os.path.relpath(image_rel)
        tileset.image_path_rel_to_map = image_rel_to_map
        tileset.image_path_rel_to_cwd = path.normpath(image_rel_to_cur)

        # act
        actual = mut.load_map_from_file_path(map_path)

        # verify
        assert_map_info_equal(self, expected, actual)
        self.assertEqual(expected, actual)

    def test_load_map_from_file_like_object(self):
        # arrange
        expected = self.expected_basic_map_info
        tileset = expected.tiles[1].tileset
        image_rel_to_map = "../basicTileset.png"
        tileset.image_path_rel_to_map = image_rel_to_map
        tileset.image_path_rel_to_cwd = None

        file_like = StringIO(map__basicMap_tileset_other_directory__as_string)

        # act
        # noinspection PyArgumentEqualDefault
        actual = mut.load_map_from_file_like(file_like, None, ".json")

        # verify
        assert_map_info_equal(self, expected, actual)
        self.assertEqual(expected, actual)

    def test_load_map_from_file_like_object_with_file_path(self):
        # arrange
        expected = self.expected_basic_map_info
        tileset = expected.tiles[1].tileset
        image_rel_to_map = "../basicTileset.png"
        tileset.image_path_rel_to_map = image_rel_to_map
        tileset.image_path_rel_to_cwd = "basicTileset.png"

        file_like = StringIO(map__basicMap_tileset_other_directory__as_string)

        # act
        actual = mut.load_map_from_file_like(file_like, "./resources/basicMap_tileset_other_directory.json")

        # verify
        assert_map_info_equal(self, expected, actual)
        self.assertEqual(expected, actual)

    def test_load_map_from_file_like_object_with_directory_path(self):
        # arrange
        expected = self.expected_basic_map_info
        tileset = expected.tiles[1].tileset
        image_rel_to_map = "../basicTileset.png"
        tileset.image_path_rel_to_map = image_rel_to_map
        tileset.image_path_rel_to_cwd = "basicTileset.png"

        file_like = StringIO(map__basicMap_tileset_other_directory__as_string)

        # act
        actual = mut.load_map_from_file_like(file_like, "./resources", ".json")

        # verify
        assert_map_info_equal(self, expected, actual)
        self.assertEqual(expected, actual)

    def test_map_loading_without_extension_or_hint_fails(self):
        # arrange
        file_path = "./resources/basicMap_as_json_without_extension"

        # act/verify
        self.assertRaises(mut.MissingFileTypeException, mut.load_map_from_file_path, file_path)

    def test_map_loading_with_hint(self):
        # arrange
        expected = self.expected_basic_map_info
        file_path = "./resources/basicMap_as_json_without_extension"

        # act
        actual = mut.load_map_from_file_path(file_path, ".json")

        # verify
        self.assertEqual(expected, actual)

    def test_map_loading_with_unknown_hint_fails(self):
        # arrange
        file_path = "./resources/basicMap_as_json_without_extension"

        # act/verify
        self.assertRaises(mut.UnknownFileExtensionException, mut.load_map_from_file_path, file_path, ".xyz")

    def test_map_loading_with_directory_path_fails(self):
        # act/verify
        self.assertRaises(mut.UnknownFileExtensionException, mut.load_map_from_file_path, "./resources")

    def test_map_loading_with_image_collection(self):
        # arrange
        expected = self.expected_basic_map_info

        # add the tile info and its own tileset for the images from the image collection
        tileset1 = mut.TilesetInfo("Collection1", 256, 160, {"im_col_prop1": "im_col_prop1_value"},
                                   os.path.normpath("../basicTileset.png"), "basicTileset.png", 5, None, 0, 0)
        expected.tiles[5] = mut.TileInfo(tileset1, 0, 0, {"tile_prop2": "tile_prop2_value"})
        expected_rel_cwd_image_path = os.path.join("resources", "tiles01_tiles8x5_size32x32_m0_s0.png")
        tileset2 = mut.TilesetInfo("Collection1", 256, 160, {"im_col_prop1": "im_col_prop1_value"},
                                   "tiles01_tiles8x5_size32x32_m0_s0.png", expected_rel_cwd_image_path, 6, None, 0, 0)
        expected.tiles[6] = mut.TileInfo(tileset2, 0, 0, {})

        # replace last tile in the map
        expected.layers[0].data_yx[2][2] = mut.TileLayerTileInfo(2 * 32, 2 * 32, expected.tiles[6], 0, -128)
        del expected.tiles[1610612738]  # there is no modified version for the last tile anymore

        # act
        actual = mut.load_map_from_file_path("./resources/basicMap_with_image_collection.json")

        # verify
        assert_map_info_equal(self, expected, actual)
        self.assertEqual(expected, actual)

    def test_map_loading_image_layer(self):
        # arrange
        expected = self.expected_basic_map_info
        img_rel_to_map = os.path.join("tsx", "tiles01_tiles8x5_size32x32_m0_s0.png")
        img_rel_to_cwd = os.path.join("resources", "tsx", "tiles01_tiles8x5_size32x32_m0_s0.png")
        image_layer_info = mut.ImageLayerInfo("Image Layer 1", 62, 65, 3, 3, True, 0.2, {},
                                              img_rel_to_map, img_rel_to_cwd)
        expected.layers.append(image_layer_info)

        # act
        actual = mut.load_map_from_file_path("./resources/basicMap_image_layer.json")

        # verify
        assert_map_info_equal(self, expected, actual)
        self.assertEqual(expected, actual)

    def test_map_loading_object_layer(self):
        # arrange
        expected = self.expected_basic_map_info
        # , x, y, width, height, rotation, id, name, properties, visible, object_type

        rect1 = mut.ObjectRectangleInfo(14.9090909090909, 9.09090909090909, 19.2727272727273, 12.5454545454545, 45, 1,
                                        "Rect1!", {"r_prop1": "R1"}, True, "R")
        # override the calculates values to make sure they are calculated correctly!
        rect1.pixel_points = [(14.9090909090909, 9.09090909090909), (28.53696705559529, 22.718785237413478),
                              (19.665991073436818, 31.589761219571955), (6.038114926932426, 17.961885073067563)]
        rect1.pixel_rect = _get_pixel_rect_from_points(rect1.pixel_points)
        rect2 = mut.ObjectRectangleInfo(6.90909090909091, 43.6363636363636, 18.1818181818182, 14.1818181818182, 0, 4,
                                        "Rect2!", {}, True, "R")
        # override the calculates values to make sure they are calculated correctly!
        rect2.pixel_points = [(6.90909090909091, 43.6363636363636), (25.09090909090911, 43.6363636363636),
                              (25.09090909090911, 57.8181818181818), (6.90909090909091, 57.8181818181818)]
        rect2.pixel_rect = _get_pixel_rect_from_points(rect2.pixel_points)

        ellipse1 = mut.ObjectEllipseInfo(42, 9.63636363636364, 26.5454545454545, 4.54545454545454, 45, 5, "Ellipse1!",
                                         {}, True, "E", True)
        ellipse1.pixel_rect = _get_pixel_rect_from_points(ellipse1.pixel_points)

        ellipse2 = mut.ObjectEllipseInfo(30.5454545454545, 33.0909090909091, 19.6363636363636, 19.6363636363636, 0, 6,
                                         "Ellipse2!", {}, True, "E", True)
        ellipse2.pixel_rect = _get_pixel_rect_from_points(ellipse2.pixel_points)

        polygon_points = [(0, 0), (-11.0909090909091, 3.45454545454545), (-12, 13.4545454545455),
                          (-7.27272727272727, 21.2727272727273), (9.63636363636364, 24.9090909090909),
                          (27.8181818181818, 22.1818181818182), (20.7272727272727, 10.1818181818182),
                          (14.1818181818182, 14.3636363636364), (15.4545454545455, 18.5454545454545),
                          (6.36363636363637, 19.6363636363636), (-2.18181818181818, 16.3636363636364),
                          (-5.45454545454545, 7.81818181818181), (3.45454545454546, 4.72727272727272),
                          (11.8181818181818, 2.36363636363636), (8.54545454545455, -5.09090909090909),
                          (-0.18181818181818, -0.181818181818187)]
        polygon1 = mut.ObjectPolygonInfo(26.7272727272727, 64.7272727272727, 0, 0, 0, 7, "Poly1!", {}, True, "P",
                                         polygon_points)
        # override the calculates values to make sure they are calculated correctly!
        polygon1.pixel_points = [(26.7272727272727, 64.7272727272727), (15.636363636363601, 68.18181818181816),
                                 (14.727272727272702, 78.1818181818182), (19.454545454545432, 86.0),
                                 (36.363636363636346, 89.6363636363636), (54.545454545454504, 86.9090909090909),
                                 (47.4545454545454, 74.9090909090909), (40.9090909090909, 79.09090909090911),
                                 (42.1818181818182, 83.27272727272721), (33.09090909090907, 84.3636363636363),
                                 (24.545454545454522, 81.09090909090911), (21.272727272727252, 72.54545454545452),
                                 (30.181818181818162, 69.45454545454542), (38.545454545454504, 67.09090909090907),
                                 (35.27272727272725, 59.63636363636361), (26.545454545454522, 64.54545454545452)]
        polygon1.pixel_rect = _get_pixel_rect_from_points(polygon1.pixel_points)

        polygon_points = [(0, 0), (-11.0909, 3.45455), (-12, 13.4545), (-7.27273, 21.2727), (9.63636, 24.9091),
                          (27.8182, 22.1818), (20.7273, 10.1818), (14.1818, 14.3636), (15.4545, 18.5455),
                          (6.36364, 19.6364), (-2.18182, 16.3636), (-5.45455, 7.81818), (3.45455, 4.72727),
                          (11.8182, 2.36364), (8.54545, -5.09091), (-0.181818, -0.181818)]
        polygon2 = mut.ObjectPolygonInfo(74.9090909090909, 59.0909090909091, 0, 0, 30, 8, "Poly2!", {}, True, "P",
                                         polygon_points)
        # override the calculates values to make sure they are calculated correctly!
        polygon2.pixel_points = [(74.9090909090909, 59.0909090909091), (63.57681475825808, 56.537187149552636),
                                 (57.789536063677645, 64.74284788612682), (57.974371974225704, 73.87724269799433),
                                 (70.79987346910312, 85.48100247631606), (87.90945879664719, 92.21001139257476),
                                 (87.7685592609521, 78.2722565471615), (80.00908998048106, 78.62105158070726),
                                 (79.02033051187752, 82.8790332167934), (70.60196480962972, 79.27835032978186),
                                 (64.83777936260594, 72.17129238827614), (66.2762220428785, 63.13437658226852),
                                 (75.53718396773444, 64.91212000145717), (83.96213233609616, 67.04698137631016),
                                 (84.85512269586064, 58.95477670252886), (74.84254090222562, 58.84254108404382)]
        polygon2.pixel_rect = _get_pixel_rect_from_points(polygon2.pixel_points)

        polyline_points = [(0, 0), (9.63636363636364, -7.63636363636364), (2, -24.7272727272727),
                           (14.7272727272727, -27.8181818181818), (9.63636363636364, -36.3636363636364),
                           (16.9090909090909, -37.0909090909091), (12, -40.7272727272727),
                           (22.3636363636364, -40.7272727272727)]
        poly_line1 = mut.ObjectPolylineInfo(59.0909090909091, 44.7272727272727, 0, 0, 0, 9, "Line1!", {}, True, "L",
                                            polyline_points)
        # override the calculates values to make sure they are calculated correctly!
        poly_line1.pixel_points = [(59.0909090909091, 44.7272727272727), (68.72727272727275, 37.09090909090906),
                                   (61.0909090909091, 19.999999999999996), (73.8181818181818, 16.9090909090909),
                                   (68.72727272727275, 8.363636363636296), (76.0, 7.636363636363598),
                                   (71.0909090909091, 4.0), (81.4545454545455, 4.0)]
        poly_line1.pixel_rect = _get_pixel_rect_from_points(poly_line1.pixel_points)

        polyline_points = [(0, 0), (6.50259496356905, -7.63636), (1.3495956903995, -24.7273),
                           (9.93795030561025, -27.8182), (6.50259496356905, -36.3636), (11.4102242442671, -37.0909),
                           (8.09757414239698, -40.7273), (15.0909090909091, -40.7273)]
        poly_line2 = mut.ObjectPolylineInfo(92.1818181818182, 6.54545454545455, 0, 0, 180, 11, "Line2!", {}, True, "L",
                                            polyline_points)
        # override the calculates values to make sure they are calculated correctly!
        poly_line2.pixel_points = [(92.1818181818182, 6.54545454545455), (85.67922321824915, 14.18181454545455),
                                   (90.8322224914187, 31.27275454545455), (82.24386787620796, 34.36365454545455),
                                   (85.67922321824915, 42.90905454545455), (80.7715939375511, 43.63635454545455),
                                   (84.08424403942122, 47.272754545454546), (77.09090909090911, 47.272754545454546)]
        poly_line2.pixel_rect = _get_pixel_rect_from_points(poly_line2.pixel_points)

        object_tile = mut.ObjectTileInfo(10.9425225283193, 104.579719128914, 15.8181818181818, 37.4876654561821,
                                         -32.1853616968773, 13, "Tile1!", {}, True, "T", 3221225474,
                                         expected.tiles[3221225474])

        objects = [
            rect1,
            rect2,
            ellipse1,
            ellipse2,
            polygon1,
            polygon2,
            poly_line1,
            poly_line2,
            object_tile,
        ]
        # noinspection SpellCheckingInspection
        object_layer_info = mut.ObjectLayerInfo("Object Layer 1", 2, 2, 3, 3, True, 1.0, {}, 0, 0, "topdown", objects)
        expected.layers.append(object_layer_info)

        # act
        actual = mut.load_map_from_file_path("./resources/basicMap_object_layer.json")

        # verify
        assert_map_info_equal(self, expected, actual)
        self.assertEqual(expected, actual)

    def test_map_loading_external_tileset_in_tsx(self):
        # arrange
        expected = self.expected_basic_map_info

        img_rel_path_to_cur = os.path.join("resources", "basicTileset.png")
        tileset = mut.TilesetInfo("basicTilesetOffset", 32, 32, {}, "basicTileset.png", img_rel_path_to_cur, 5, None, 0,
                                  0)
        # noinspection PyArgumentEqualDefault
        _tiles = {
            5: mut.TileInfo(tileset, 5 + 0 * 32, 5 + 0 * 32, {}),
            6: mut.TileInfo(tileset, 5 + 1 * 32, 5 + 0 * 32, {}),
            7: mut.TileInfo(tileset, 5 + 2 * 32, 5 + 0 * 32, {}),
            8: mut.TileInfo(tileset, 5 + 3 * 32, 5 + 0 * 32, {}),
        }
        _tiles[5].probability = 0.5
        _tiles[7].probability = 0.5
        expected.tiles.update(_tiles)

        img_rel_path_to_cur = os.path.join("resources", "basicTileset.png")
        tileset = mut.TilesetInfo("basicTilesetProperties", 32, 32, {}, "basicTileset.png", img_rel_path_to_cur, 9,
                                  None, 0, 0)
        # noinspection PyArgumentEqualDefault
        _tiles = {
            9: mut.TileInfo(tileset, 5 + 0 * 32, 5 + 0 * 32, {}),
            10: mut.TileInfo(tileset, 5 + 1 * 32, 5 + 0 * 32, {}),
            11: mut.TileInfo(tileset, 5 + 2 * 32, 5 + 0 * 32, {}),
            12: mut.TileInfo(tileset, 5 + 3 * 32, 5 + 0 * 32, {}),
        }
        expected.tiles.update(_tiles)

        # properties on tileset 'basicTilesetProperties
        expected.tiles[9].tileset.properties = {"p4": "p44"}
        expected.tiles[9].properties = {"p3": "p33"}

        # act
        actual = mut.load_map_from_file_path("./resources/basicMap_tsx.json")

        # verify
        assert_map_info_equal(self, expected, actual)
        self.assertEqual(expected, actual)

    def test_map_loading_external_animated_tileset_in_tsx(self):
        # arrange
        expected = self.expected_basic_map_info

        img_rel_path_to_cur = os.path.join("resources", "basicTileset.png")
        tileset = mut.TilesetInfo("basicTilesetAnimation", 32, 32, {}, "basicTileset.png", img_rel_path_to_cur, 5, None,
                                  0, 0)
        # noinspection PyArgumentEqualDefault
        _tiles = {
            5: mut.TileInfo(tileset, 5 + 0 * 32, 5 + 0 * 32, {}),
            6: mut.TileInfo(tileset, 5 + 1 * 32, 5 + 0 * 32, {}),
            7: mut.TileInfo(tileset, 5 + 2 * 32, 5 + 0 * 32, {}),
            8: mut.TileInfo(tileset, 5 + 3 * 32, 5 + 0 * 32, {}),
        }
        expected.tiles.update(_tiles)
        expected.tiles[7].properties = {"p9": "p99"}
        expected.tiles[7].probability = 0.75
        expected.tiles[7].animation = [mut.AnimationFrameInfo(expected.tiles[6], 500),
                                       mut.AnimationFrameInfo(expected.tiles[8], 250)]

        tileset = mut.TilesetInfo("basicTilesetTransparentColor", 32, 32, {}, "basicTileset.png", img_rel_path_to_cur,
                                  9, "ff00ff", 0, 0)
        # noinspection PyArgumentEqualDefault
        _tiles = {
            9: mut.TileInfo(tileset, 5 + 0 * 32, 5 + 0 * 32, {}),
            10: mut.TileInfo(tileset, 5 + 1 * 32, 5 + 0 * 32, {}),
            11: mut.TileInfo(tileset, 5 + 2 * 32, 5 + 0 * 32, {}),
            12: mut.TileInfo(tileset, 5 + 3 * 32, 5 + 0 * 32, {}),
        }
        expected.tiles.update(_tiles)

        # act
        actual = mut.load_map_from_file_path("./resources/basicMap_animated_tsx.json")

        # verify
        assert_map_info_equal(self, expected, actual)
        self.assertEqual(expected, actual)

    def test_map_loading_external_image_collection_as_tsx(self):
        # arrange
        expected = self.expected_basic_map_info

        # add the tile info and its own tileset for the images from the image collection
        tileset1 = mut.TilesetInfo("Collection1", 138, 42, {"im_col_prop1": "im_col_prop1_value"},
                                   os.path.normpath("../basicTileset.png"), "basicTileset.png", 5, None, 0, 0)
        expected.tiles[5] = mut.TileInfo(tileset1, 0, 0, {"tile_prop2": "tile_prop2_value"})
        expected_rel_cwd_image_path = os.path.join("resources", "tiles01_tiles8x5_size32x32_m0_s0.png")
        tileset2 = mut.TilesetInfo("Collection1", 256, 160, {"im_col_prop1": "im_col_prop1_value"},
                                   "tiles01_tiles8x5_size32x32_m0_s0.png", expected_rel_cwd_image_path, 6, None, 0, 0)
        expected.tiles[6] = mut.TileInfo(tileset2, 0, 0, {})

        # act
        actual = mut.load_map_from_file_path("./resources/basicMap_image_collection_tsx.json")

        # verify
        assert_map_info_equal(self, expected, actual)
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()
