# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pytmxloader
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import unittest

import pytmxloader.pytmxloader as mut  # module under test

__version__ = '4.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class ComparisonTestsShould(unittest.TestCase):
    def setUp(self):
        self.sut = None
        self.equal_other = None
        self.different_other = object()
        self.other_type = object()

    def test_equal_other_true(self):
        # act/verify
        self.assertTrue(self.sut == self.equal_other)

    def test_equal_other_first_true(self):
        # act/verify
        self.assertTrue(self.equal_other == self.sut)

    def test_equal_different_false(self):
        # act/verify
        self.assertFalse(self.sut == self.different_other)

    def test_equal_different_first_false(self):
        # act/verify
        self.assertFalse(self.different_other == self.sut)

    def test_equal_other_type_false(self):
        # act/verify
        self.assertFalse(self.sut == self.other_type)

    def test_equal_other_type_first_false(self):
        # act/verify
        self.assertFalse(self.other_type == self.sut)

    def test_not_equal_other_false(self):
        # act/verify
        self.assertFalse(self.sut != self.equal_other)

    def test_not_equal_other_first_false(self):
        # act/verify
        self.assertFalse(self.equal_other != self.sut)

    def test_not_equal_different_true(self):
        # act/verify
        self.assertTrue(self.sut != self.different_other)

    def test_not_equal_different_first_true(self):
        # act/verify
        self.assertTrue(self.different_other != self.sut)

    def test_not_equal_other_type_true(self):
        # act/verify
        self.assertTrue(self.sut != self.other_type)

    def test_not_equal_other_type_first_true(self):
        # act/verify
        self.assertTrue(self.other_type != self.sut)

    # def test_hash_equality(self):
    #     # arrange
    #     expected = 1
    #     s = set([self.sut, self.equal_other])
    #
    #     # act
    #     actual = len(s)
    #
    #     # verify
    #     self.assertEqual(expected, actual)


class MapInfoEqualityShould(ComparisonTestsShould):
    def setUp(self):
        self.layers = [4, 2, 3, 5]
        self.tiles = {1: 'a', 3: 'b', 2: 'c'}
        self.sut = mut.MapInfo(self.layers, self.tiles, {})
        self.equal_other = mut.MapInfo([4, 2, 3, 5], {1: 'a', 3: 'b', 2: 'c'}, {})
        self.different_other = mut.MapInfo([1, 2, 3, 4], {1: 'b', 2: 'r', 4: '9'}, {})
        self.other_type = object()

    def test_all_values_have_been_set_correctly(self):
        # act/verify
        self.assertEqual(self.layers, self.sut.layers)
        self.assertEqual(self.tiles, self.sut.tiles)


class TileLayerInfoEqualityShould(ComparisonTestsShould):
    def setUp(self):
        data = "bla"
        self.sut = mut.TileLayerInfo("name", 33, 44, 200, 350, 32, 64, True, 0.55, data, {}, 0, 0)
        self.equal_other = mut.TileLayerInfo("name", 33, 44, 200, 350, 32, 64, True, 0.55, data, {}, 0, 0)
        self.different_other = mut.TileLayerInfo("name", 3, 4, 20, 35, 33, 65, False, 0.11, "buh!", {}, 0, 0)
        self.other_type = object()

    def test_all_values_have_been_set_correctly(self):
        # act/verify
        self.assertEqual("name", self.sut.name)
        self.assertEqual(33, self.sut.pixel_offset_x)
        self.assertEqual(44, self.sut.pixel_offset_y)
        self.assertEqual(200, self.sut.width)
        self.assertEqual(350, self.sut.height)
        self.assertEqual(32, self.sut.tile_w)
        self.assertEqual(64, self.sut.tile_h)
        self.assertEqual(True, self.sut.visible)
        self.assertEqual(0.55, self.sut.opacity)
        self.assertEqual("bla", self.sut.data_yx)
        # noinspection SpellCheckingInspection
        self.assertEqual("tilelayer", self.sut.layer_type)


class LayerInfoEqualityShould(ComparisonTestsShould):
    def setUp(self):
        self.sut = mut.LayerInfo("layer_type", {}, "name", True, 0.55, 33, 44, 3, 4, 32, 64)
        self.equal_other = mut.LayerInfo("layer_type", {}, "name", True, 0.55, 33, 44, 3, 4, 32, 64)
        self.different_other = mut.LayerInfo("different", {}, "name", True, 0.57, 23, 54, 5, 6, 32, 64)
        self.other_type = object()

    def test_all_values_have_been_set_correctly(self):
        # arrange
        expected = "layer_type"

        # act/verify
        self.assertEqual(expected, self.sut.layer_type)


class TileInfoEqualityShould(ComparisonTestsShould):
    def setUp(self):
        self.tileset = object()
        self.sut = mut.TileInfo(self.tileset, 333, 444, {1: 'a'}, -45, flip_y=True)
        self.equal_other = mut.TileInfo(self.tileset, 333, 444, {1: 'a'}, -45, flip_y=True)
        self.different_other = mut.TileInfo(object(), 33, 44, {3: 'b'}, 145, True)
        self.other_type = object()

    def test_all_values_have_been_set_correctly(self):
        # act/verify
        self.assertEqual(self.tileset, self.sut.tileset)
        self.assertEqual(False, self.sut.flip_x)
        self.assertEqual(True, self.sut.flip_y)
        self.assertEqual(-45, self.sut.angle)
        self.assertEqual({1: 'a'}, self.sut.properties)
        self.assertEqual(333, self.sut.spritesheet_x)
        self.assertEqual(444, self.sut.spritesheet_y)

    def test_animation_pointing_to_self_causes_no_recursion(self):
        # arrange
        expected = mut.TileInfo(self.tileset, 333, 444, {1: 'a'}, -45, flip_y=True)
        actual = mut.TileInfo(self.tileset, 333, 444, {1: 'a'}, -45, flip_y=True)
        expected.animation = [mut.AnimationFrameInfo(expected, 222)]
        actual.animation = [mut.AnimationFrameInfo(actual, 222)]

        # act/verify
        self.assertEqual(expected, actual)


class TileLayerTileInfoEqualityShould(ComparisonTestsShould):
    def setUp(self):
        self.tile_info = object()
        self.sut = mut.TileLayerTileInfo(3, 5, self.tile_info, 9, 99)
        self.equal_other = mut.TileLayerTileInfo(3, 5, self.tile_info, 9, 99)
        self.different_other = mut.TileLayerTileInfo(13, 15, object, 19, 199)
        self.other_type = object()

    def test_all_values_have_been_set_correctly(self):
        # act/verify
        self.assertEqual(self.tile_info, self.sut.tile_info)
        self.assertEqual(3, self.sut.tile_x)
        self.assertEqual(5, self.sut.tile_y)
        self.assertEqual(9, self.sut.offset_x)
        self.assertEqual(99, self.sut.offset_y)


class TileSetInfoEqualityShould(ComparisonTestsShould):
    def setUp(self):
        self.sut = mut.TilesetInfo("name", 32, 64, {3: "props"}, "img_name", "", 333, "#ff00ff", 0, 0)
        self.equal_other = mut.TilesetInfo("name", 32, 64, {3: "props"}, "img_name", "", 333, "#ff00ff", 0, 0)
        self.different_other = mut.TilesetInfo("name2", 16, 56, {5: "props"}, "img_name2", "", 66, "#0000ff", 1, 2)
        self.other_type = object()

    def test_all_values_have_been_set_correctly(self):
        # act/verify
        self.assertEqual("name", self.sut.name)
        self.assertEqual(32, self.sut.tile_width)
        self.assertEqual(64, self.sut.tile_height)
        self.assertEqual({3: "props"}, self.sut.properties)
        self.assertEqual("img_name", self.sut.image_path_rel_to_map)
        self.assertEqual(333, self.sut.first_gid)
        self.assertEqual("#ff00ff", self.sut.transparent_color)


class ObjectRectangleInfoEqualityShould(ComparisonTestsShould):
    def setUp(self):
        self.sut = mut.ObjectRectangleInfo(14.9090909090909, 9.09090909090909, 19.2727272727, 12.5454545455, 45, 5,
                                           "name", {}, False, "R")
        self.equal_other = mut.ObjectRectangleInfo(14.9090909090909, 9.09090909090909, 19.2727272727, 12.5454545455, 45,
                                                   5, "name", {}, False, "R")
        self.different_other = mut.ObjectRectangleInfo(1, 2, 3, 4, 50, 5, "name", {}, False, "R")
        self.other_type = object()

    def test_all_values_have_been_set_correctly(self):
        # act/verify
        precision = 4
        self.assertEqual([round(x, precision) for x in self.sut.pixel_rect],
                         [round(x, precision) for x in
                          (6.038114926932426, 9.09090909090909, 22.498852128662865, 22.498852128662865)])
        self.assertEqual([(round(x, precision), round(y, precision)) for x, y in self.sut.pixel_points],
                         [(round(x, precision), round(y, precision)) for x, y in
                          [(14.9090909090909, 9.09090909090909), (28.53696705559529, 22.718785237413478),
                           (19.665991073436818, 31.589761219571955), (6.038114926932426, 17.961885073067563)]])


class ObjectEllipseInfoEqualityShould(ComparisonTestsShould):
    def setUp(self):
        self.sut = mut.ObjectEllipseInfo(42, 9.63636363636364, 26.5454545454545, 4.54545454545454, 45, 6, "name", {},
                                         False, "T", True)
        self.equal_other = mut.ObjectEllipseInfo(42, 9.63636363636364, 26.5454545454545, 4.54545454545454, 45, 6,
                                                 "name", {},
                                                 False, "T", True)
        self.different_other = mut.ObjectEllipseInfo(42, 9.63636363636364, 26.5454545454545, 4.54545454545454, 0, 6,
                                                     "name", {},
                                                     False, "T", True)
        self.other_type = object()

    def test_all_values_have_been_set_correctly(self):
        # act/verify
        precision = 4
        self.assertEqual([round(x, precision) for x in self.sut.pixel_rect],
                         [round(x, precision) for x in
                          (38.78587826733388, 9.63636363636364, 21.984592651436266, 21.984592651436262)])
        self.assertEqual([(round(x, precision), round(y, precision)) for x, y in self.sut.pixel_points],
                         [(round(x, precision), round(y, precision)) for x, y in
                          [(42.0, 9.63636363636364), (60.770470918770144, 28.40683455513378),
                           (57.55634918610402, 31.620956287799903), (38.78587826733388, 12.850485369029762)]])


class ObjectPolygonInfoEqualityShould(ComparisonTestsShould):
    def setUp(self):
        points = [(0, 0), (-11.0909, 3.45455), (-12, 13.4545), (-7.27273, 21.2727), (9.63636, 24.9091),
                  (27.8182, 22.1818), (20.7273, 10.1818), (14.1818, 14.3636), (15.4545, 18.5455),
                  (6.36364, 19.6364), (-2.18182, 16.3636), (-5.45455, 7.81818), (3.45455, 4.72727),
                  (11.8182, 2.36364), (8.54545, -5.09091), (-0.181818, -0.181818)]
        self.sut = mut.ObjectPolygonInfo(74.9090909090909, 59.0909090909091, 0, 0, 30, 55, "name", {}, True, "P",
                                         points)
        self.equal_other = mut.ObjectPolygonInfo(74.9090909090909, 59.0909090909091, 0, 0, 30, 55, "name", {}, True,
                                                 "P", points)
        self.different_other = mut.ObjectPolygonInfo(3, 4, 0, 0, 4, 55, "", {}, False, "", [])
        self.other_type = object()

    def test_all_values_have_been_set_correctly(self):
        # act/verify
        precision = 4
        self.assertEqual([round(x, precision) for x in self.sut.pixel_rect],
                         [round(x, precision) for x in
                          (57.789536063677645, 56.537187149552636, 30.119922732969542, 35.67282424302212)])
        self.assertEqual([(round(x, precision), round(y, precision)) for x, y in self.sut.pixel_points],
                         [(round(x, precision), round(y, precision)) for x, y in
                          [(74.9090909090909, 59.0909090909091), (63.57681475825808, 56.537187149552636),
                           (57.789536063677645, 64.74284788612682), (57.974371974225704, 73.87724269799433),
                           (70.79987346910312, 85.48100247631606), (87.90945879664719, 92.21001139257476),
                           (87.7685592609521, 78.2722565471615), (80.00908998048106, 78.62105158070726),
                           (79.02033051187752, 82.8790332167934), (70.60196480962972, 79.27835032978186),
                           (64.83777936260594, 72.17129238827614), (66.2762220428785, 63.13437658226852),
                           (75.53718396773444, 64.91212000145717), (83.96213233609616, 67.04698137631016),
                           (84.85512269586064, 58.95477670252886), (74.84254090222562, 58.84254108404382)]])


class ObjectPolyLineEqualityShould(ComparisonTestsShould):
    def setUp(self):
        points = [(0, 0), (6.50259496356905, -7.63636), (1.3495956903995, -24.7273), (9.93795030561025, -27.8182),
                  (6.50259496356905, -36.3636), (11.4102242442671, -37.0909), (8.09757414239698, -40.7273),
                  (15.0909090909091, -40.7273)]
        self.sut = mut.ObjectPolylineInfo(92.1818181818182, 6.54545454545455, 0, 0, 180, 345, "n", {}, False, "t",
                                          points)
        self.equal_other = mut.ObjectPolylineInfo(92.1818181818182, 6.54545454545455, 0, 0, 180, 345, "n", {}, False,
                                                  "t", points)
        self.different_other = mut.ObjectPolylineInfo(2, 3, 0, 0, 45, 554, "", {}, False, "", [])
        self.other_type = object()

    def test_all_values_have_been_set_correctly(self):
        # act/verify
        precision = 4
        self.assertEqual([round(x, precision) for x in self.sut.pixel_rect],
                         [round(x, precision) for x in
                          (77.09090909090911, 6.54545454545455, 15.090909090909093, 40.7273)])
        self.assertEqual([(round(x, precision), round(y, precision)) for x, y in self.sut.pixel_points],
                         [(round(x, precision), round(y, precision)) for x, y in
                          [(92.1818181818182, 6.54545454545455), (85.67922321824915, 14.18181454545455),
                           (90.8322224914187, 31.27275454545455), (82.24386787620796, 34.36365454545455),
                           (85.67922321824915, 42.90905454545455), (80.7715939375511, 43.63635454545455),
                           (84.08424403942122, 47.272754545454546), (77.09090909090911, 47.272754545454546)]])


class ObjectTileInfoEqualityShould(ComparisonTestsShould):
    def setUp(self):
        self.sut = mut.ObjectTileInfo(10.9425225283193, 104.579719128914, 15.8181818181818, 37.4876654561821,
                                      -32.1853616968773, 445, "n", {}, True, "*", 43434, None)
        self.equal_other = mut.ObjectTileInfo(10.9425225283193, 104.579719128914, 15.8181818181818, 37.4876654561821,
                                              -32.1853616968773, 445, "n", {}, True, "*", 43434, None)
        self.different_other = mut.ObjectTileInfo(2, 3, 67, 54, 33, 443, "", {}, True, "", 99999, None)
        self.other_type = object()

    def test_all_values_have_been_set_correctly(self):
        # act/verify
        precision = 4
        self.assertEqual([round(x, precision) for x in self.sut.pixel_rect],
                         [round(x, precision) for x in
                          (-9.025659890672662, 64.42709636982696, 33.35557286785364, 40.15262275908704)])
        self.assertEqual([(round(x, precision), round(y, precision)) for x, y in self.sut.pixel_points],
                         [(round(x, precision), round(y, precision)) for x, y in
                          [(10.9425225283193, 104.579719128914), (-9.025659890672662, 72.85281016764755),
                           (4.361730558189015, 64.42709636982696), (24.329912977180975, 96.1540053310934)]])


if __name__ == '__main__':
    unittest.main()
