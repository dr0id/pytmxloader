# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pytmxloader
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""



__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

import unittest
import datadriventestingdecorators as ddtd
import pytmxloader.tileset_image_generator as mut  # module under test

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


@ddtd.use_data_driven_testing_decorators
class MyTestCase(unittest.TestCase):

    @ddtd.test_case(2, 2, 32, 32, 0, 0, [(0, 0), (32, 0), (0, 32), (32, 32)], test_name="default_values")
    @ddtd.test_case(2, 2, 32, 32, 10, 0, [(10, 10), (42, 10), (10, 42), (42, 42)])
    @ddtd.test_case(2, 2, 32, 32, 10, 3, [(10, 10), (45, 10), (10, 45), (45, 45)])
    def test_something(self, num_tiles_x, num_tiles_y, tile_width, tile_height, margin, spacing, expected):
        # arrange

        # act
        actual = mut.get_tile_coordinates(num_tiles_x, num_tiles_y, tile_width, tile_height, margin, spacing)

        # verify
        self.assertEqual(expected, actual)

    def test_verbose_and_quiet_at_same_time(self):
        # arrange
        # act
        # verify: -v and -q not allowed together
        self.assertRaises(SystemExit, mut.main, ["-v", "-q", "bla.png", "3", "3"])

if __name__ == '__main__':
    unittest.main()
